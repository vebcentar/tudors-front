import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import Proizvod from "../components/Proizvod/Proizvod";
import { Helmet } from "react-helmet";

import ProductsContext from "../context/productsContext/productstContext";
import axios from "axios";
import { useHistory, useLocation } from "react-router-dom";

import ReactPaginate from "react-paginate";
import { BounceLoader } from "react-spinners";

const url = `${process.env.REACT_APP_TUDORS_API}api/products/getAllProducts`;

const Proizvodi = (props) => {
  //filters state
  const [selectedSizes, setSelectedSizes] = useState([]);
  const [selectedColors, setSelectedColors] = useState([]);
  const [selectedPrice, setSelectedPrice] = useState([
    false,
    false,
    false,
    false,
  ]);
  const [sort, setSort] = useState(0);
  // minimum and maximum aveilable price
  const [availablePrice, setAvailablePrice] = useState();

  //data for filter
  const [sizes, setSizes] = useState([]);
  const [colors, setColors] = useState([]);

  //pagination
  const [pagination, setPagination] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [numberOfProducts, setNumberOfProducts] = useState(0);

  //other state
  const [showScroll, setShowScroll] = useState(false);
  const [loading, setLoading] = useState(true);
  const [showMoreVelicina, setShowMoreVelicina] = useState(false);
  const [showMoreBoja, setShowMoreBoja] = useState(false);
  const [showFilterMobile, setShowFilterMobile] = useState(false);
  const [catForBread, setCatForBread] = useState(null);
  const [prodCategory, setProdCategory] = useState("");
  const [breadArr, setBreadArr] = useState(null);

  // const [category_id, setCategory_id] = useState(null);

  //hooks
  const {
    products,
    cart,
    loadProducts,
    searchedProducts,
    categories,
    loadCategories,
  } = useContext(ProductsContext);
  const history = useHistory();
  const location = useLocation();
  const { state, search } = location;

  const id = state
    ? state.id
    : search.substring(search.indexOf("=") + 1, search.indexOf("&"));
  const name = state
    ? state.name
    : decodeURI(search.substring(search.lastIndexOf("=") + 1, search.length));

  //use effects
  //scroll to top
  useEffect(() => {
    scrollTop();
  }, [products]);

  useEffect(() => {
    if (categories) {
      createBreadCrump(id);
    }
  }, [products]);

  useEffect(() => {
    fetchSizes();
    fetchColors();
  }, [id]);

  //reset filters
  useEffect(() => {
    resetFilters();
  }, [id]);

  //filter products
  useEffect(() => {
    if (
      selectedColors.length === 0 &&
      selectedSizes.length === 0 &&
      selectedPrice[0] === false &&
      selectedPrice[1] === false &&
      selectedPrice[2] === false &&
      selectedPrice[3] === false &&
      sort === 0
    ) {
      if (id) {
        const data = {
          offset: currentPage === 1 ? 0 : currentPage * 12,
          limit: 12,
          category_id: id,
        };
        fetchProducts(url, data);
      } else {
        fetchProducts(searchedProducts);
      }
    } else {
      const [min, max] = minMax();
      const obj = {
        offset: currentPage === 1 ? 0 : currentPage * 12,
        category_id: id,
        limit: 12,
        sortPrice: sort * 1,
        colors: `${selectedColors}`,
        priceRange: JSON.stringify({ minRange: min, maxRange: max }),
        sizes: JSON.stringify(selectedSizes),
      };

      const urlAll = `${process.env.REACT_APP_TUDORS_API}api/products/getAllFilterProducts`;
      fetchProducts(urlAll, obj);
    }
  }, [selectedColors, selectedSizes, selectedPrice, currentPage, id, sort]);

  // **************** AXIOS ****************
  const fetchSizes = async () => {
    const urlSizes = `${process.env.REACT_APP_TUDORS_API}api/products/getFilterSizes`;
    try {
      const res = await axios.post(urlSizes, { category_id: id });

      setSizes(res.data.sizes);
    } catch (err) {
      console.log(err);
    }
  };

  const fetchColors = async () => {
    const urlColors = `${process.env.REACT_APP_TUDORS_API}api/products/getFilterColors`;
    try {
      const res = await axios.post(urlColors, { category_id: id });
      setColors(res.data.colors);
    } catch (err) {
      console.log(err);
    }
  };

  //scrol to top funcionality
  const checkScrollTop = () => {
    if (!showScroll && window.pageYOffset > 400) {
      setShowScroll(true);
    } else if (showScroll && window.pageYOffset <= 400) {
      setShowScroll(false);
    }
  };
  const scrollTop = () => {
    const to = document.getElementById("sort").getBoundingClientRect().top;
    console.log(to);
    window.scrollTo({ top: to || 300, behavior: "smooth" });
  };
  window.addEventListener("scroll", checkScrollTop);

  const resetFilters = () => {
    setSelectedColors([]);
    setSelectedSizes([]);
    setSelectedPrice([false, false, false, false]);
  };

  const fetchProducts = async (url, data) => {
    setLoading(true);
    try {
      const res = await axios.post(url, data);
      loadProducts(res.data.products);
      setCatForBread(res.data.cat);
      setNumberOfProducts(res.data.total);
      paginationFactory();
      setAvailablePrice({ min: res.data.min, max: res.data.max });
      setLoading(false);
      // createBreadCrump(data.category_id);
    } catch (err) {}
  };

  //set min and max price for filter
  const minMax = () => {
    let min, max;
    if (selectedPrice[0]) {
      min = 10;
    } else if (selectedPrice[1]) {
      min = 15;
    } else if (selectedPrice[2]) {
      min = 20;
    } else if (selectedPrice[3]) {
      min = 30;
    } else {
      min = 0;
    }

    //max
    if (selectedPrice[3]) {
      max = 10000;
    } else if (selectedPrice[2]) {
      max = 30;
    } else if (selectedPrice[1]) {
      max = 20;
    } else if (selectedPrice[0]) {
      max = 15;
    } else {
      max = 0;
    }
    return [min, max];
  };

  const sizeHandler = (size) => {
    const currentSelected = [...selectedSizes];
    const found = currentSelected.find((s) => s === size);
    if (found) {
      const index = currentSelected.findIndex((i) => i === found);
      currentSelected.splice(index, 1);
    } else {
      currentSelected.push(size);
    }

    setSelectedSizes(currentSelected);
  };

  const colorHandler = (color) => {
    const currentSelected = [...selectedColors];
    const found = currentSelected.find((s) => s === color);
    if (found) {
      const index = currentSelected.findIndex((i) => i === found);
      currentSelected.splice(index, 1);
    } else {
      currentSelected.push(color);
    }

    setSelectedColors(currentSelected);
  };
  const priceHandler = (idx, reset = false) => {
    let cur = [...selectedPrice];
    cur = [false, false, false, false, false];
    if (!reset) {
      cur[idx] = true;
    }
    setSelectedPrice(cur);
  };

  const paginationFactory = () => {
    let pagination = [];
    if (numberOfProducts > 12) {
      for (let i = 0; i < numberOfProducts / 12; i++) {
        pagination.push(i);
      }
    }
    setPagination(pagination);
  };

  const handleChangeSort = (val) => {
    setSort(val * 1);
  };

  const makeBreadCump = () => {
    if (catForBread) {
      const string = location.pathname;
      const array = string.split("/");
      const htmlToReturn = (
        <>
          {array[2] ? (
            <a>{array[2].charAt(0).toUpperCase() + array[2].slice(1)}</a>
          ) : null}
          {array[3] ? (
            <>
              <span className="divider"> / </span>
              <a>{array[3].charAt(0).toUpperCase() + array[3].slice(1)}</a>
            </>
          ) : null}
        </>
      );
      return htmlToReturn;
    }
  };

  const createBreadCrump = (prod) => {
    const prodId = prod;
    const lozaArr = [];

    if (categories.length) {
      categories.forEach((cat) => {
        if (cat.id === prodId) {
          lozaArr.push(cat);
        } else if (cat.children) {
          cat.children.forEach((chilCat) => {
            if (chilCat.id === prodId) {
              lozaArr.push(cat);
              lozaArr.push(chilCat);
            }
          });
        }
      });
      if (lozaArr) {
        console.log(lozaArr);
        setProdCategory(lozaArr[lozaArr.length - 1].name);
        setBreadArr(lozaArr);
      }
    }
  };

  useEffect(() => {
    console.log(products);
  });

  return (
    <>
      {/*<Helmet>
        <title>Tudors | Proizvodi</title>
      </Helmet>*/}
      <div
        className="intro-banner"
        style={{
          backgroundImage: `url(${require("../img/proizvodi/intro-banner.png")})`,
        }}
      ></div>
      <main>
        <div className="proizvodi-section">
          <div className="container pt-50 pb-80">
            <div className="row">
              <div className="col-12">
                <div className="title">
                  <h1>{prodCategory}</h1>

                  <div className="category">
                    {breadArr && (
                      <>
                        <Link to="/">Početna</Link>
                        {breadArr.map((item) => (
                          <Link
                            key={item.id}
                            to={{
                              pathname: `/proizvodi/${item.slug}`,
                              state: { id: item.id, name: item.name },
                              search: `id=${item.id}&name=${item.name}`,
                            }}
                          >{`/${item.name}`}</Link>
                        ))}
                      </>
                    )}
                    {/* <Link to='/proizvodi'>{name}</Link> */}
                    {/* <span className='divider'> / </span>
                     <Link to='/proizvodi'>Slim fit</Link>
                    <span className='divider'> / </span>
                    <Link to='/proizvodi'>Dugi rukav</Link> */}
                  </div>
                </div>

                <div className="line"></div>
              </div>
            </div>

            <div className="row row-2">
              <div className="col-lg-3 col-md-3">
                <div className="left-side m-h-2">
                  {/* <h3>Kategorije</h3>

                   <ul className="link-list">
                    <li>
                      <Link to="/proizvodi">
                        <p>SLIM FIT</p>
                      </Link>

                      <ul>
                        <li>
                          <Link to="/proizvodi">Dugi rukav</Link>
                        </li>

                        <li>
                          <Link to="/proizvodi">Kratki rukav</Link>
                        </li>
                      </ul>
                    </li>
                  </ul>

                  <ul className="link-list">
                    <li>
                      <Link to="/proizvodi">
                        <p>KLASIK FIT</p>
                      </Link>

                      <ul>
                        <li>
                          <Link to="/proizvodi">Dugi rukav</Link>
                        </li>

                        <li>
                          <Link to="/proizvodi">Kratki rukav</Link>
                        </li>
                      </ul>
                    </li>
                  </ul>
 */}
                  <h3 id="velicina">VELIČINA</h3>
                  <ul>
                    {
                      <>
                        {sizes.map((size, idx) => {
                          if (idx > 4 && !showMoreVelicina) return;
                          return (
                            <li key={idx}>
                              <div className="d-flex align-items-center">
                                <input
                                  type="checkbox"
                                  id={size.name}
                                  name={size.name}
                                  checked={
                                    selectedSizes.find((i) => i === size.id) ||
                                    false
                                  }
                                  onChange={() => sizeHandler(size.id)}
                                />
                                <label htmlFor={size.name}>{size.name} </label>
                              </div>
                            </li>
                          );
                        })}
                        {sizes.length > 4 && (
                          <h4
                            style={{ cursor: "pointer" }}
                            onClick={() => setShowMoreVelicina((p) => !p)}
                          >
                            {showMoreVelicina
                              ? "Prikaži manje"
                              : "Prikaži više"}
                          </h4>
                        )}
                      </>
                    }
                  </ul>

                  <h3>BOJA</h3>
                  <ul>
                    {
                      <>
                        {colors.map((item, idx) => {
                          if (idx > 4 && !showMoreBoja) return;
                          return (
                            <li key={idx}>
                              <div className="d-flex align-items-center">
                                <input
                                  type="checkbox"
                                  id={item.color}
                                  name={item.color}
                                  checked={
                                    selectedColors.find(
                                      (i) => i === item.color
                                    ) || false
                                  }
                                  onChange={() => colorHandler(item.color)}
                                />
                                <label htmlFor={item.color}>{item.color}</label>
                              </div>
                            </li>
                          );
                        })}
                        {colors.length > 4 && (
                          <h4
                            style={{ cursor: "pointer" }}
                            onClick={() => setShowMoreBoja((p) => !p)}
                          >
                            {showMoreBoja ? "Prikaži manje" : "Prikaži više"}
                          </h4>
                        )}
                      </>
                    }
                  </ul>

                  <h3>Cijena</h3>

                  <ul className="cijene">
                    {availablePrice &&
                      availablePrice.min < 15 &&
                      availablePrice.max >= 10 && (
                        <li>
                          <div className="d-flex align-items-center">
                            <input
                              type="checkbox"
                              id="cijena-1"
                              name="cijena-1"
                              checked={selectedPrice[0]}
                              onChange={() => priceHandler(0)}
                            />
                            <label htmlFor="cijena-1">10 - 15€</label>
                          </div>
                        </li>
                      )}
                    {availablePrice &&
                      availablePrice.min < 20 &&
                      availablePrice.max >= 15 && (
                        <li>
                          <div className="d-flex align-items-center">
                            <input
                              type="checkbox"
                              id="cijena-2"
                              name="cijena-2"
                              checked={selectedPrice[1]}
                              onChange={() => priceHandler(1)}
                            />
                            <label htmlFor="cijena-2">15 - 20€</label>
                          </div>
                        </li>
                      )}
                    {availablePrice &&
                      availablePrice.min < 30 &&
                      availablePrice.max >= 20 && (
                        <li>
                          <div className="d-flex align-items-center">
                            <input
                              type="checkbox"
                              id="cijena-3"
                              name="cijena-3"
                              checked={selectedPrice[2]}
                              onChange={() => priceHandler(2)}
                            />
                            <label htmlFor="cijena-3">20 - 30€</label>
                          </div>
                        </li>
                      )}
                    {availablePrice && availablePrice.max >= 30 && (
                      <li>
                        <div className="d-flex align-items-center">
                          <input
                            type="checkbox"
                            id="cijena-5"
                            name="cijena-5"
                            checked={selectedPrice[3]}
                            onChange={() => priceHandler(3)}
                          />
                          <label htmlFor="cijena-5">30€ +</label>
                        </div>
                      </li>
                    )}
                    {/* {availablePrice && availablePrice.min < 100 && availablePrice.max >= 70 && (
                      <li>
                        <div className='d-flex align-items-center'>
                          <input
                            type='checkbox'
                            id='cijena-4'
                            name='cijena-4'
                            checked={selectedPrice[3]}
                            onChange={() => priceHandler(3)}
                          />
                          <label htmlFor='cijena-4'>70 - 100€</label>
                        </div>
                      </li>
                    )} */}

                    <li>
                      <div className="d-flex align-items-center">
                        <input
                          type="checkbox"
                          id="sve"
                          name="sve"
                          checked={
                            selectedPrice[0] === false &&
                            selectedPrice[1] === false &&
                            selectedPrice[2] === false &&
                            selectedPrice[3] === false &&
                            selectedPrice[4] === false
                          }
                          onChange={() => priceHandler(null, true)}
                        />
                        <label htmlFor="sve">Sve</label>
                      </div>
                    </li>
                  </ul>

                  <div className="line d-h-2"></div>
                </div>

                <div className="filteri">
                  <button
                    id="filteri-btn"
                    onClick={() => setShowFilterMobile(true)}
                  >
                    <span>Filteri</span>
                  </button>

                  <div
                    className="filteri-content"
                    style={{
                      transform: showFilterMobile ? "scale(1,1)" : "scale(1,0)",
                    }}
                    id="filteri-content"
                  >
                    <h3>Kategorije</h3>

                    {/* <label htmlFor='slim-fit'>SLIM FIT</label>
                    <select name='slim-fit' value='dugi-rukavi' onChange={() => {}} id='slim-fit'>
                      <option value disabled hidden>
                        Izaberite
                      </option>
                      <option value='dugi-rukavi'>Dugi rukav</option>
                      <option value='Kratki-rukavi'>Kratki rukav</option>
                    </select> */}

                    {/* <label htmlFor='klasik-fit'>KLASIK FIT</label>
                    <select name='klasik-fit' value='0' onChange={() => {}} id='klasik-fit'>
                      <option value='0' disabled hidden>
                        Izaberite
                      </option>
                      <option value='dugi-rukavi'>Dugi rukav</option>
                      <option value='Kratki-rukavi'>Kratki rukav</option>
                    </select> */}

                    <label htmlFor="velicina">VELIČINA</label>
                    <select
                      name="velicina"
                      value="0"
                      onChange={() => {}}
                      id="velicina"
                    >
                      <option value="0" disabled hidden>
                        Izaberite velicinu
                      </option>
                      <option value="xs">XS</option>
                      <option value="s">S</option>
                      <option value="m">M</option>
                      <option value="l">L</option>
                      <option value="xl">XL</option>
                      <option value="xxl">XXL</option>
                    </select>

                    <label htmlFor="boja">Boja</label>
                    <select name="boja" id="boja" value="0" onChange={() => {}}>
                      <option value="0" disabled hidden>
                        Izaberite boju
                      </option>
                      <option value="bijela">Bijela</option>
                      <option value="crna">Crna</option>
                      <option value="crvena">Crvena</option>
                      <option value="plava">Plava</option>
                      <option value="zelena">Zelena</option>
                      <option value="zuta">Žuta</option>
                    </select>

                    <label htmlFor="cijena">Cijena</label>
                    <select
                      name="cijena"
                      value="0"
                      onChange={() => {}}
                      id="cijena"
                    >
                      <option value="0" disabled hidden>
                        Izaberite cijenu
                      </option>
                      <option value="cijena-1">10 - 15€</option>
                      <option value="cijena-2">15 - 20€</option>
                      <option value="cijena-3">20 - 30€</option>
                      <option value="cijena-4">30+</option>
                      {/* <option value='cijena-5'>100€ +</option> */}
                    </select>

                    <button id="primjeni-btn">Primjeni</button>

                    <button
                      id="zatvori-btn"
                      onClick={() => setShowFilterMobile(false)}
                    >
                      Zatvori
                    </button>
                  </div>
                </div>
              </div>

              <div
                className="col-lg-9 col-md-9 right-side"
                style={{ marginTop: "-100px" }}
              >
                <div className="row mb-40">
                  <div className="col-lg-12 col-md-12 col-sm-12 col-12 d-flex align-items-center kategorija-sort-align">
                    <div className="kategorija-sort" id="sort">
                      <label htmlFor="sort-cijena">Sortiraj po:</label>
                      <select
                        id="sort-cijena"
                        value={sort}
                        onChange={(e) => handleChangeSort(e.target.value)}
                      >
                        <option value="0" disabled hidden>
                          Podrazumijevano
                        </option>
                        <option value={2}>Cijena uzlazno</option>
                        <option value={1}>Cijena silazno</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div className="row" id="proizvodi">
                  {loading ? (
                    <div
                      style={{
                        width: "100%",
                        display: "flex",
                        justifyContent: "center",
                        marginTop: "30px",
                      }}
                    >
                      <BounceLoader color="#eaeaea" size={220} />
                    </div>
                  ) : products.length ? (
                    products.map((product) => (
                      <Proizvod
                        key={product.id}
                        id={`${product.id}`}
                        link={product.product_code}
                        cover={product.cover}
                        price={
                          product.discount
                            ? (
                                (1 - product.discount / 100) *
                                product.price
                              ).toFixed(2)
                            : product.price
                        }
                        oldPrice={product.discount ? product.price : null}
                        name={product.name}
                      />
                    ))
                  ) : (
                    <div className="col-12 mt-5">
                      <h1 className="text-center mt-5">
                        Za odabrane filtere nema proizvoda
                      </h1>
                    </div>
                  )}
                </div>

                <div className="row">
                  <div className="col-12">
                    <nav aria-label="..." className="proizvodi-nav mt-60">
                      {/* <ul className='pagination'>
                        <li className='page-item mr-10 disabled d-none'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>
                            <svg
                              aria-hidden='true'
                              focusable='false'
                              data-prefix='fas'
                              data-icon='chevron-left'
                              className='svg-inline--fa fa-chevron-left fa-w-10'
                              role='img'
                              xmlns='http://www.w3.org/2000/svg'
                              viewBox='0 0 320 512'
                              width='15px'
                              height='15px'>
                              <path
                                fill='black'
                                d='M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z'></path>
                            </svg>
                          </Link>
                        </li> */}
                      {/* <div className='pagination-numbers'> */}
                      {/* {pagination.map((page) => (
                            <li
                              key={page}
                              className={`page-item mx-10 ${page === currentPage && "active"}`}>
                              <button
                                onClick={() => setCurrentPage(page)}
                                className='page-link shadow-none'
                                to=''
                                tabIndex='-1'>
                                <span>{page + 1}</span>
                              </button>
                            </li>
                          ))} */}

                      {/* </div> */}
                      {/* {formatPagination(pagination)} */}

                      {/* <li className='page-item mx-10 active'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>
                            <span>1</span>
                            <span className='sr-only'>(current)</span>
                          </Link>
                        </li>
                        <li className='page-item mx-10'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>f
                            <span>2</span>
                          </Link>
                        </li>
                        <li className='page-item mx-10'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>
                            <span>3</span>
                          </Link>
                        </li>
                        <li className='page-item mx-10'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>
                            <span>4</span>
                          </Link>
                        </li>
                        <li className='page-item mx-10'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>
                            <span>5</span>
                          </Link>
                        </li>
                        <li className='page-item ml-10'>
                          <Link className='page-link shadow-none' to='' tabIndex='-1'>
                            <span>»</span>
                          </Link>
                        </li> */}
                      {/* </ul> */}
                      {numberOfProducts > 12 && (
                        <ReactPaginate
                          previousLabel=""
                          nextLabel=""
                          breakLabel={"..."}
                          breakClassName="page-item"
                          pageCount={numberOfProducts / 12 - 1}
                          pageRangeDisplayed={5}
                          marginPagesDisplayed={1}
                          onPageChange={(e) => {
                            scrollTop();
                            setCurrentPage(e.selected + 1);
                          }}
                          containerClassName="pagination"
                          pageClassName="page-item"
                          pageLinkClassName="page-link"
                          activeClassName={"active"}
                        />
                      )}
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Proizvodi;
