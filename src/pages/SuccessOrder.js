import React, { useEffect } from 'react';
import mainImg from '../img/o-nama/mainImg.png';
import { Helmet } from 'react-helmet';

const ONama = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div>
      {/*<Helmet>
        <title>Tudors | O nama</title>
      </Helmet>*/}
      <main>
        <div className='o-nama-section'>
          <div className='container pt-50 pb-80'>
            <div className='row'>
              <div className='col-12'>
                <div className='title'>
                  <h1>O nama</h1>
                </div>

                <div className='line'></div>

                <div className='text-container'>
                  <h1 className='text-center'>
                    Uspješna narudžba. Hvala na vašoj narudžbi. Svi podaci sa
                    detaljima narudžbe poslani su na vašu email adresu.
                  </h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default ONama;
