import React from "react";
import { useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";

const ConfirmRegistration = () => {
  const history = useHistory();
  return (
    <div style={{ minHeight: "calc(100vh - 250px)" }}>
      {/*<Helmet>
        <title>Tudors | Potvrda registracije</title>
      </Helmet>*/}
      <div
        className="intro-banner "
        style={{
          backgroundImage: `url(${require("../img/proizvodi/intro-banner.png")})`,
        }}
      ></div>
      <div className="d-flex align-items-center flex-column">
        <h2 className="text-center mt-5 mb-5">
          Proverite svoj mail kako bi potvrdili registraciju!
        </h2>

        <button
          style={{
            width: 300,
            height: "5rem",
            border: "2px solid #71041b",
            color: "#71041b",
            fontSize: "1.6rem",
          }}
          className="mt-5"
          onClick={() => history.push("/")}
        >
          Početna
        </button>
      </div>
    </div>
  );
};

export default ConfirmRegistration;
