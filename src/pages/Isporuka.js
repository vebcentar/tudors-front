import React, { useContext, useState, useEffect } from "react";
import { ProizvodIsporuka } from "../components/Proizvod/ProizvodIsporuka";
import ProductsContext from "../context/productsContext/productstContext";
import AuthContext from "../context/authContext/authContext";
import { Helmet } from "react-helmet";

import axios from "axios";
import { Link } from "react-router-dom";

const url = `${process.env.REACT_APP_TUDORS_API}`;
const tokenTest =
  "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI3IiwianRpIjoiYjFhYzJmNmQwOTcxZmUwOWIyNzNmYzNlYTI1ZjE0NDU0YTkyOGU0ODAxOWNmZDMwYjhiM2Y1MDRmMDkxYzAxMjhkYjQ4NDY5ODVkMTY1ODIiLCJpYXQiOjE2MDEzMTUxODAsIm5iZiI6MTYwMTMxNTE4MCwiZXhwIjoxNjMyODUxMTgwLCJzdWIiOiI3NiIsInNjb3BlcyI6WyJhZG1pbiJdfQ.HsVYhCpkEApqjN8yc-IaKTuZZbmyBsuMlrcSO2ZDQHTsdYoqtG5P_-EFJ4KrAGrx8b-aShqTJbQ_ccQQpju8D-JXqWwZ1zcMBV2emQpsk930-EKHk9VQWGl89o6JTxsMdHjRQzyvfFGixJmhGy81LP-tvf4uod88olFzCQCn7BKcaYJCbE1qtE_hlDRw51y8db9yRfofzKv4oRcfI3Kl0wrZkJwMCotgnNBuXSeCYDj062Ua2Q2KRmPHi7fPb4DKI79FcQqHf_29rGAlTK80iDX0KM9iaEhzNdLk7FhAWGtMMZXCSz7BG_pfkjEAR-fLQqgWI3A3WiswebhL9nekJfPb6gPgShqDFqrwSuc5QqEF7OTIVMSlphQyTAkDqHDDMDyAHVZ94irhgpuzdAgKMksT2BtVkUEP2xET0I8ry4Px7_YCzqivGZlihwGf98O9j8bQbmiSAU2gp-U9yylTMrzKsqw6I5mDE23f008FgOpk_Ji5gc5xrkQf2I5WtSUPXsw80tYVircIHdwlFv67eultKNRJBJMaG3jWq9hxdZe0j6gFSVgcbqjFSJ23fPgocvfZtLug8Y5pj4HJCqrTuvJUTwoW1irkPp-9xfjnu4bG5kf6zcVzIlHQ4nyvTPZ2emWELQT42VzpzoMjjqKCow0rKp7zCQLlcVgwceOJvoE";
const Isporuka = () => {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [, setZip] = useState("");
  const [, setAppartment] = useState("");
  const [postNumber, setPostNumber] = useState("");
  const [customer_id, setCustomer_id ] = useState("");

  const [payment, setPayment] = useState(1);
  const [alertDisplay, setAlertDisplay] = useState("none");
  const [alertText, setAlertText] = useState("");
  const [type] = useState(0);
  const [createAccount, setCreateAccount] = useState(false);
  const [accept, setAccept] = useState(false);
  const [dodatneNapomene, setDodatneNapomene] = useState("");

  const { cart, removeFromCart, clearCart } = useContext(ProductsContext);
  const { user, isAuthencated } = useContext(AuthContext);

  useEffect(() => {
    if (isAuthencated && user) {
      console.log(user)
      setCustomer_id(user.id);
      setName(user.name);
      setAddress(user.address);
      setAppartment(user.apartment);
      setCity(user.city);
      setZip(user.zip_code);
      setEmail(user.email);
      setPhone(user.phone);
    }else{
      setCustomer_id('1');
    }

    const inputType = JSON.parse(localStorage.getItem("inputType"));
    if (inputType && inputType.dodatneNapomene) {
      setDodatneNapomene(inputType.dodatneNapomene);
    }
  }, []);

  const handleSubmit = () => {
    var cityPost = city + " " + postNumber;
    const config = {
      header: {
        "Content-Type": "application/json",
        Authorization: tokenTest,
      },
    };
    const items = cart.map((item) => ({
      product_id: item.id,
      product_code: item.product_code,
      product_color: item.color,
      size_id: item.sizeId,
      amount: item.amount,
    }));


    const formData = {
      name,
      email,
      phone,
      address,
      city: cityPost,
      type,
      payment_type: payment,
      order_items: JSON.stringify(items),
      customer_id,
    };
    console.log(formData);
    if (
      name !== "" &&
      address !== "" &&
      city !== "" &&
      email !== "" &&
      phone !== ""
    ) {
      if (accept) {
        try {
          axios
            .post(`${url}api/orders/addOrder`, formData, config)
            .then((data) => {
              /*             ;
               */ console.log("SUCCESS ORDER");
              console.log(data);
              setTimeout(() => {
              window.location.href = "./success-order";
              }, 1500);

              clearCart();
            })
            .catch((error) => {
              console.log("ERROR ORDER");
              console.log(error);
              //setAlertDisplay("flex");
              //setAlertText("DESILA SE GREŠKA. POKUŠAJTE PONOVO .");
            });
        } catch (error) {
          console.log("ERRO");
          console.log(error);
          //setAlertDisplay("flex");
          //setAlertText("DESILA SE GREŠKA. POKUŠAJTE PONOVO .");
          //TODO
          //PROBLEM SA KREIRANJEM ISPORUKE
        }
      } else {
        setAlertDisplay("flex");
        setAlertText("MOLIM VAS POTVRDITE USLOVE KUPOVINE");
      }
    } else {
      //TODO
      // LOGIC FOR SHOWING ERROR
      setAlertDisplay("flex");
      setAlertText("MOLIM VAS POPUNITE SVA POLJA");
    }
  };
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {});

  const totalPrice = cart.reduce(function (earnings, item) {
    return (
      earnings +
      +(item.discount_value
        ? ((1 - item.discount_value / 100) * item.price).toFixed(2)
        : item.price) *
        item.amount
    );
  }, 0);

  const terms = () => {
    const userInput = {
      type,
      payment,
      dodatneNapomene,
    };

    localStorage.setItem("inputType", JSON.stringify(userInput));
  };
  return (
    <main>
      {/*<Helmet>
        <title>Tudors | Isporuka</title>
      </Helmet>*/}
      <div className="isporuka-section">
        <div className="container pt-50 pb-80">
          <div className="row">
            <div className="col-12">
              <div className="title">
                <h1>Isporuka</h1>
              </div>

              <div className="red-line"></div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-6">
              <div className="licni-podaci">
                {/*  <div className="nalog d-flex flex-column mb-30">
                  <span className="mb-2">
                    Imate nalog?
                    <button data-toggle="modal" data-target="#modal-prijava">
                      Prijavite se!
                    </button>
                  </span>
                  <span className="my-2">
                    Nemate nalog?
                    <button
                      data-toggle="modal"
                      data-target="#modal-registracija"
                    >
                      Registrujte se!
                    </button>
                  </span>
                </div>

                <div className="line my-25"></div>
 */}
                <div className="kupujte-group">
                  <div className="kupujte">
                    <h2>Kupujte kao gost</h2>
                  </div>

                  <div className="radio-lica d-flex align-items-center">
                    <div className="radio-fizicka-lica d-flex align-items-center">
                      <input
                        type="radio"
                        id="fizicka-lica"
                        name="isporuka-lice"
                        checked
                        onChange={() => {}}
                      />
                      <label htmlFor="fizicka-lica" className="ml-10">
                        Fizičko lice
                      </label>
                    </div>
                    <div className="radio-pravna-lica d-flex align-items-center ml-30">
                      <input
                        type="radio"
                        id="pravna-lica"
                        name="isporuka-lice"
                      />

                      <label htmlFor="pravna-lica" className="ml-10">
                        Pravno lice
                      </label>
                    </div>
                  </div>
                </div>

                <form>
                  <label>Ime i prezime *</label>
                  <input
                    type="text"
                    name="ime"
                    placeholder=""
                    required
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />

                  {/* <!-- Pravna lica --> */}
                  <label className="pravno-lice">Naziv firme *</label>
                  <input
                    className="pravno-lice"
                    type="text"
                    name="naziv-firme"
                    placeholder=""
                    required
                  />
                  <div className="pib-pdv d-flex flex-row justify-content-between">
                    <div className="pib">
                      <label className="pravno-lice">PIB *</label>
                      <input
                        className="input-half pravno-lice"
                        type="text"
                        name="pib"
                        placeholder=""
                        required
                      />
                    </div>

                    <div className="pdv">
                      <label className="pravno-lice">PDV *</label>
                      <input
                        className="input-half pravno-lice"
                        type="text"
                        name="pdv"
                        placeholder=""
                        required
                      />
                    </div>
                  </div>
                  {/* <!-- End --> */}

                  <label>Adresa i broj * *</label>
                  <input
                    type="text"
                    name="adresa-broj"
                    placeholder=""
                    required
                    value={address}
                    onChange={(e) => setAddress(e.target.value)}
                  />
                  <label>Grad</label>
                  <input
                    type="text"
                    name="grad-postanski-broj"
                    placeholder=""
                    required
                    value={city}
                    onChange={(e) => setCity(e.target.value)}
                  />
                  <label>Poštanski broj *</label>
                  <input
                    type="number"
                    name="grad-postanski-broj"
                    placeholder=""
                    required
                    value={postNumber}
                    onChange={(e) => setPostNumber(e.target.value)}
                  />
                  <label>Email *</label>
                  <input
                    type="email"
                    name="email"
                    placeholder=""
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <label>Telefon *</label>
                  <input
                    type="text"
                    name="telefon"
                    placeholder=""
                    required
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                  />

                  {/*<div className="kreirajte-nalog">
                    <input
                      type="checkbox"
                      id="kreirajte-nalog"
                      name="kreirajte-nalog"
                      checked={createAccount}
                      onChange={() => setCreateAccount((s) => !s)}
                    />
                    <label htmlFor="kreirajte-nalog">
                      Kreirajte nalog sa ovim podacima
                    </label>
                  </div>*/}

                  <div className="y-line my-30"></div>

                  <div className="nacin-placanja">
                    <div>
                      <span>Način plaćanja:</span>
                    </div>
                    <div className="input-group-container">
                      <div className="input-container">
                        <input
                          type="radio"
                          id="pouzece"
                          name="nacin-pl"
                          checked={payment === 1}
                          onChange={() => setPayment(1)}
                        />
                        <label htmlFor="pouzece">Pouzećem</label>
                      </div>
                     {/* <div className="input-container">
                        <input
                          type="radio"
                          id="platna-kartica"
                          name="nacin-pl"
                          checked={payment === 2}
                          onChange={() => setPayment(2)}
                        />
                        <label htmlFor="platna-kartica">Platnom karticom</label>
                </div> */}
                    </div>
                  </div>

                  <div className="y-line my-30"></div>

                  <label>Dodatne napomene</label>
                  <textarea
                    name="napomena"
                    value={dodatneNapomene}
                    onChange={(e) => setDodatneNapomene(e.target.value)}
                    placeholder="Ukoliko imate bilo kakve dodatne napomene upišite ih ovde."
                  ></textarea>
                </form>
              </div>
            </div>

            <div className="col-lg-6">
              <div className="content-w">
                <div className="row">
                  <div className="col-12">
                    <div className="vasa-korpa">
                      <h3>Korpa</h3>
                    </div>
                  </div>
                </div>

                {cart.map((item) => {
                  return (
                    <ProizvodIsporuka
                      name={item.name}
                      code={item.product_code}
                      cover={item.image}
                      price={
                        item.discount_value
                          ? (
                              (1 - item.discount_value / 100) *
                              item.price
                            ).toFixed(2)
                          : item.price
                      }
                      amount={item.amount}
                      size={item.size}
                      remove={() => removeFromCart(item.id)}
                      key={item.id}
                    />
                  );
                })}

                <div className="row">
                  <div className="col-12">
                    <div className="ukupna-cijena">
                      <div className="ukupno">
                        Ukupno:
                        <span className="cijena">
                          {totalPrice.toFixed(2)} €
                        </span>
                      </div>
                      <div className="dostava">
                        Cijena dostave pouzećem:
                        <span className="cijena">3.00€</span>
                      </div>
                      <div className="pdv">
                        <span>
                          Cijene su sa uračunatim PDV-om. <br />
                          Isporuku vršimo samo na teritoriji Crne Gore.
                        </span>
                      </div>
                    </div>

                    <div className="r-line my-30"></div>

                    <div className="ukupno-za-placanje">
                      <span>
                        Ukupno za plaćanje:{" "}
                        <span className="cijena">
                          {(totalPrice + 3).toFixed(2)} €
                        </span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row row-ch">
            <div className="col-12">
              <div className="uslovi-kupovine-container py-40">
                <input
                  type="checkbox"
                  id="uslovi-kupovine"
                  name="uslovi-kupovine"
                  checked={accept}
                  onChange={() => setAccept((s) => !s)}
                />
                <label htmlFor="uslovi-kupovine">
                  Pročitao / la sam i prihvatam{" "}
                  <button onClick={terms}>
                    <Link target="_blank" to="./uslovi-kupovine">
                      {" "}
                      uslove kupovine.
                    </Link>
                  </button>
                </label>
              </div>
              <h3 style={{ color: "#71041b", display: alertDisplay }}>
                {alertText}
              </h3>

              <br />

              <div className="potvrdite">
                <button
                  className="tr-3"
                  data-toggle="modal"
                  data-target="#modal-uspjesna-kupovina"
                  onClick={handleSubmit}
                >
                  POTVRDITE
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default Isporuka;
