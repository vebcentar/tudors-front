import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import axios from 'axios';
import ProductsContext from '../context/productsContext/productstContext';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import { useState } from 'react';
import introBanner from '../img/naslovna/intro-banner.png';
import '../styles/slider.css';
const url = `${process.env.REACT_APP_TUDORS_API}`;
const Index = () => {
  const { specialProducts, loadSpecialProducts, categories } = useContext(
    ProductsContext
  );

  console.log(categories);
  const [slideImages, setSlideImages] = useState([]);
  useEffect(() => {
    window.scrollTo(0, 0);
    axios({
      method: 'post',
      url: 'https://admin.tudorsshop.me/api/pages/getPage',
      data: {
        slug: 'slider'
      }
    })
      .then((res) => setSlideImages(res.data.page.images))
      .catch((err) => console.log(err));
  }, []);
  useEffect(() => {
    // console.log(slideImages);
  }, [slideImages]);

  useEffect(() => {
    fetchProducts();
  }, []);
  useEffect(() => {
    /*console.log("SPECIJALNI PROIZVODI");
    console.log(specialProducts);*/
  });
  const fetchProducts = async () => {
    try {
      const res = await axios.post(`${url}api/products/specialOffers`, {});
      await loadSpecialProducts(res.data.products);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {/*<Helmet>
        <title>Tudors | Naslovna</title>
      </Helmet>*/}
      <div className="intro-bg mt-5 pt-5 mt-lg-0 pt-lg-0 ">
        <div className="slide-container mt-5 pt-5 mt-xl-0 pt-xl-0">
          <Slide>
            {slideImages.map((item, idx) => {
              return (
                <div className="each-slide " key={idx}>
                  <div>
                    <img
                      className="col-12"
                      src={`https://admin.tudorsshop.me/${item.image}`}
                      alt=""
                    />
                  </div>
                </div>
              );
            })}
            {/* <div className="each-slide ">
              <div>
                <img className="col-12" src={introBanner} alt="" />
              </div>
            </div>
            <div className="each-slide ">
              <div>
                <img className="col-12" src={introBanner} alt="" />
              </div>
            </div>
            <div className="each-slide ">
              <div>
                <img className="col-12" src={introBanner} alt="" />
              </div>
            </div> */}
          </Slide>
        </div>
        <div className="triangle-bottom-left"></div>
      </div>

      <div className="kategorije-section">
        <div className="container">
          <div id="carousel-kategorije">
            {categories.length && (
              <OwlCarousel
                items={3}
                loop
                className="owl-carousel owl-theme"
                responsive={{
                  0: {
                    items: 1,
                    nav: false,
                    dots: true,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"/></svg>'
                    ]
                  },
                  576: {
                    items: 1,
                    nav: false,
                    dots: true,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"/></svg>'
                    ]
                  },
                  867: {
                    items: 2,
                    nav: false,
                    dots: true,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"/></svg>'
                    ]
                  },
                  991: {
                    items: 3,
                    nav: false,
                    dots: true,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z"/></svg>'
                    ]
                  }
                }}
              >
                {categories
                  .filter(
                    (el) =>
                      //el.id === 580 || kategorija majice
                      el.id === 584 ||
                      el.id === 588 ||
                      el.id === 589 ||
                      el.id === 614 ||
                      el.id === 632
                  )
                  .map((category, idx) => {
                    return (
                      <div className="item" key={category.name}>
                        <div className="item-container">
                          <div className="card-container card-hover card-1 tr-3">
                            <div
                              className="card-front front-bg tr-3"
                              style={{
                                backgroundImage: `url(${require(`../img/naslovna/${category.id}.jpg`)})`
                              }}
                            >
                              <div className="card-body">
                                <div className="kategorija">
                                  <div className="kolekcija">
                                    <span>Kolekcija</span>
                                    <span>
                                      <b>
                                        ljeto <span className="n-f-b">2022</span>
                                      </b>
                                    </span>
                                  </div>
                                  <div className="red-line"></div>
                                  <div className="naslov">
                                    <h3>{category.name}</h3>
                                  </div>
                                </div>
                              </div>

                              <div className="card-back tr-3">
                                <div className="card-body">
                                  <div className="content-container">
                                    <div className="kategorija-back">
                                      <div className="kolekcija">
                                        <span>Kolekcija</span>
                                        <span>
                                          <b>
                                            ljeto{' '}
                                            <span className="n-f-b">2022</span>
                                          </b>
                                        </span>
                                      </div>
                                      <div className="red-line"></div>
                                      <div className="naslov">
                                        <h3>{category.name}</h3>
                                      </div>
                                    </div>
                                    {/* <ul className='list'>
                                      {category.children.map((item, idx) => {
                                        if (idx < 7) {
                                          return (
                                            <li key={item.id}>
                                              <Link
                                                to={{
                                                  pathname: `/proizvodi/kosulje/${item.slug}`,
                                                  state: {
                                                    id: item.id,
                                                    name: item.name,
                                                  },
                                                  search: `id=${item.id}&name=${item.name}`,
                                                }}>
                                                {item.name}
                                              </Link>
                                            </li>
                                          );
                                        }
                                      })}
                                    </ul> */}

                                    <Link
                                      to={{
                                        pathname: `/proizvodi/${category.slug}`,
                                        state: {
                                          id: category.id,
                                          name: category.name
                                        },
                                        search: `id=${category.id}&name=${category.name}`
                                      }}
                                    >
                                      <div className="cijela-kolekcija">
                                        <button>Cijela kolekcija</button>
                                      </div>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
              </OwlCarousel>
            )}
          </div>
        </div>
      </div>

      <div className="section-spacer"></div>

      <div className="section-divider">
        <div className="container">
          <div className="divider"></div>
        </div>
      </div>

      <main>
        <div className="izdvajamo-iz-ponude-section">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="section-title">
                  <h2>
                    Izdvajamo <span>iz ponude</span>
                  </h2>
                </div>
              </div>
            </div>

            {/* <div id='carousel-izdvajamo-iz-ponude' className='owl-carousel owl-theme'> */}
            {/* <div> */}
            {specialProducts.length ? (
              <OwlCarousel
                id="carousel-izdvajamo-iz-ponude"
                items={specialProducts.length - 1}
                className="owl-carousel owl-theme d-flex flex-column"
                loop={true}
                margin={10}
                nav={true}
                center={true}
                dots={false}
                responsive={{
                  0: {
                    items: 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  },
                  374: {
                    items:
                      specialProducts.length > 2
                        ? 2
                        : specialProducts.length - 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  },
                  576: {
                    items:
                      specialProducts.length > 2
                        ? 2
                        : specialProducts.length - 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  },
                  660: {
                    items:
                      specialProducts.length > 3
                        ? 3
                        : specialProducts.length - 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  },
                  860: {
                    items:
                      specialProducts.length > 4
                        ? 4
                        : specialProducts.length - 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  },
                  991: {
                    items:
                      specialProducts.length > 5
                        ? 5
                        : specialProducts.length - 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  },
                  1400: {
                    items:
                      specialProducts.length > 5
                        ? 5
                        : specialProducts.length - 1,
                    nav: true,
                    dots: false,
                    navText: [
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                    ]
                  }
                }}
                navText={[
                  '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                  '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>'
                ]}
              >
                {specialProducts.map((data, idx) => {
                  return (
                    <div
                      key={idx}
                      className="item   mx-auto"
                      style={{ maxWidth: '200px' }}
                    >
                      <Link to={`/proizvod-detalji/${data.id}`}>
                        <div className="content-container">
                          <div className="top-line"></div>

                          <div
                            className="img-container"
                            style={{
                              backgroundImage: `url(https://admin.tudorsshop.me/${data.image})`
                            }}
                          ></div>

                          <div className="text-container">
                            <div className="proizvod">
                              <span>{data.name}</span>
                            </div>

                            <div className="cijena-proizvoda">
                              <span className="cijena">
                                {data.discount_value
                                  ? (
                                      (1 - data.discount_value / 100) *
                                      data.price
                                    ).toFixed(2)
                                  : data.price}
                                €
                              </span>
                              <span className="separator">
                                {data.special_offer > 0 ? ' / ' : null}
                              </span>
                              <span className="popust">
                                {data.discount_value ? data.price : null}
                              </span>
                            </div>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
                {/* <div className='item'>
                  <Link to=''>
                    <div className='content-container'>
                      <div className='top-line'></div>

                      <div
                        className='img-container'
                        style={{
                          backgroundImage: `url(${require("../img/naslovna/ponuda-img-1.png")})`,
                        }}></div>

                      <div className='text-container'>
                        <div className='proizvod'>
                          <span>Košulja, slim fit,</span>
                          <span>dugi rukav</span>
                        </div>

                        <div className='cijena-proizvoda'>
                          <span className='cijena'>28.00€</span>
                          <span className='separator'> / </span>
                          <span className='popust'>35.00€</span>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div> */}
              </OwlCarousel>
            ) : (
              <p>Ucitavanje...</p>
            )}
          </div>
        </div>

        <div className="aksesoari-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-6">
                <div className="item-container">
                  <div className="card-container card-hover card-1 tr-3">
                    <div
                      className="card-front front-bg tr-3"
                      style={{
                        backgroundImage: `url(${require('../img/naslovna/aksesoari-img-1.png')})`
                      }}
                    >
                      <div className="card-body">
                        <div className="gradient"></div>

                        <div className="kategorija">
                          <div className="red-line"></div>
                          <div className="naslov">
                            <h4>Kaiševi</h4>
                          </div>
                        </div>
                      </div>

                      <div className="card-back tr-3">
                        <div className="card-body">
                          <div className="content-container">
                            <div className="kategorija-back">
                              <div className="naslov">
                                <h4>Kaiševi</h4>
                              </div>
                              <div className="red-line"></div>
                            </div>

                            <Link
                              to={{
                                pathname: `/proizvodi/aksesoari/kais`,
                                state: { id: 596, name: 'Kaiš' },
                                search: `id=596&name=Kaiš`
                              }}
                            >
                              <div className="cijela-kolekcija">
                                <button>Cijela kolekcija</button>
                              </div>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-6 col-sm-6">
                <div className="item-container">
                  <div className="card-container card-hover card-1 tr-3">
                    <div
                      className="card-front front-bg tr-3"
                      style={{
                        backgroundImage: `url(${require('../img/naslovna/aksesoari-img-2.png')})`
                      }}
                    >
                      <div className="card-body">
                        <div className="gradient"></div>

                        <div className="kategorija">
                          <div className="red-line"></div>
                          <div className="naslov">
                            <h4>Mašne</h4>
                          </div>
                        </div>
                      </div>

                      <div className="card-back tr-3">
                        <div className="card-body">
                          <div className="content-container">
                            <div className="kategorija-back">
                              <div className="naslov">
                                <h4>Mašne</h4>
                              </div>
                              <div className="red-line"></div>
                            </div>

                            <Link
                              to={{
                                pathname: `/proizvodi/aksesoari/leptir-masne`,
                                state: { id: 590, name: 'Leptir mašne' },
                                search: `id=590&name=Leptir mašne`
                              }}
                            >
                              <div className="cijela-kolekcija">
                                <button>Cijela kolekcija</button>
                              </div>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-6 col-sm-6">
                <div className="item-container">
                  <div className="card-container card-hover card-1 tr-3">
                    <div
                      className="card-front front-bg tr-3"
                      style={{
                        backgroundImage: `url(${require('../img/naslovna/aksesoari-img-3.png')})`
                      }}
                    >
                      <div className="card-body">
                        <div className="gradient"></div>

                        <div className="kategorija">
                          <div className="red-line"></div>
                          <div className="naslov">
                            <h4>Parfemi</h4>
                          </div>
                        </div>
                      </div>

                      <div className="card-back tr-3">
                        <div className="card-body">
                          <div className="content-container">
                            <div className="kategorija-back">
                              <div className="naslov">
                                <h4>Parfemi</h4>
                              </div>
                              <div className="red-line"></div>
                            </div>

                            <Link
                              to={{
                                pathname: `/proizvodi/aksesoari/parfem`,
                                state: { id: 602, name: 'Perfem' },
                                search: `id=602&name=Perfem`
                              }}
                            >
                              <div className="cijela-kolekcija">
                                <button>Cijela kolekcija</button>
                              </div>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-lg-3 col-md-6 col-sm-6">
                <div className="item-container">
                  <div className="card-container card-hover card-1 tr-3">
                    <div
                      className="card-front front-bg tr-3"
                      style={{
                        backgroundImage: `url(${require('../img/naslovna/aksesoari-img-4.png')})`
                      }}
                    >
                      <div className="card-body">
                        <div className="gradient"></div>

                        <div className="kategorija">
                          <div className="red-line"></div>
                          <div className="naslov">
                            <h4>Novčanici</h4>
                          </div>
                        </div>
                      </div>

                      <div className="card-back tr-3">
                        <div className="card-body">
                          <div className="content-container">
                            <div className="kategorija-back">
                              <div className="naslov">
                                <h4>Novčanici</h4>
                              </div>
                              <div className="red-line"></div>
                            </div>

                            <Link
                              to={{
                                pathname: `/proizvodi/aksesoari/novcanik`,
                                state: { id: 597, name: 'Novcanik' },
                                search: `id=597&name=Novcanik`
                              }}
                            >
                              <div className="cijela-kolekcija">
                                <button>Cijela kolekcija</button>
                              </div>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="kupovina-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-6 c-col">
                <div className="content-container c-bd">
                  <div className="icon-container">
                    <img
                      src={require('../img/naslovna/icon-besplatna-dostava.png')}
                      alt="dostava"
                    />
                  </div>

                  <div className="text-container">
                    <span className="text-1">Besplatna dostava</span>
                    <span className="text-2">za sve porudžbine iznad 30€</span>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-4 col-sm-6 c-col">
                <div className="content-container c-bd">
                  <div className="icon-container">
                    <img
                      src={require('../img/naslovna/icon-sigurna-kupovina.png')}
                      alt="kupovina"
                    />
                  </div>

                  <div className="text-container">
                    <span className="text-1">SIGURNA KUPOVINA</span>
                    <span className="text-2">online preko našeg web shopa</span>
                  </div>
                </div>
              </div>

              <div className="col-lg-4 col-md-4 col-sm-12 c-col">
                <div className="content-container c-bd">
                  <div className="icon-container">
                    <img
                      src={require('../img/naslovna/icon-brza-isporuka.png')}
                      alt="isporuka"
                    />
                  </div>

                  <div className="text-container">
                    <span className="text-1">BRZA ISPORUKA</span>

                    <span className="text-2">
                      na Vašu ili na željenu adresu
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Index;
