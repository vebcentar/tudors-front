import React, { useEffect } from "react";
import { Helmet } from "react-helmet";

const UsloviKupovine = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div>
      {/*<Helmet>
        <title>Tudors | Uslovi kupovine</title>
      </Helmet>*/}
      <div
        className='intro-banner'
        style={{
          backgroundImage: `url(${require("../img/uslovi-kupovine/intro-banner.png")})`,
        }}></div>

      <main>
        <div className='uslovi-kupovine-section'>
          <div className='container pt-50 pb-80'>
            <div className='row'>
              <div className='col-12'>
                <div className='title'>
                  <h1>Uslovi kupovine</h1>
                </div>

                <div className='line'></div>

                <div className='text-container'>
                  <h3>USLOVI KORIŠĆENJA SAJTA</h3>

                  <p>
                    Ukoliko koristite ovaj sajt, potvrđujete da ste pročitali, razumjeli i
                    prihvatili uslove korišćenja navedene u daljem tekstu i da ste u saglasnosti da
                    ste obavezni da ih poštujete, kao i sve druge primjenljive zakone i propise.
                    Uslovi korišćenja se mogu izmijeniti u svakom trenutku, te bi bilo poželjno da
                    sajt posjetite s vremena na vrijeme i provjerite da li je tekst uslova ažuriran.
                    Ukoliko ne prihvatate ove uslove, nemojte koristiti ovaj internet sajt. Određene
                    odredbe ovih Uslova mogu biti poništene eksplicitnim zakonskim aktima ili
                    obavještenjima objavljenim na drugim stranicama ovog sajta.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>UPOTREBA SADRŽAJA SAJTA</h3>

                  <p>
                    Sav materijal na ovom veb sajtu je zaštićen i svaka njegova zloupotreba može
                    značiti kršenje Zakona o autorskim i srodnim pravima, Zakona o žigovima ili
                    drugih zakona iz oblasti intelektualne svojine. Pod uslovom da se sve
                    informacije o autorskim pravima ili druge zaštićene informacije sadržane u
                    originalnom dokumentu i da se čuvaju u svim primjercima, sav sadržaj može se
                    pregledati, kopirati, odštampati ili preuzeti samo u lične, nekomercijalne i
                    informativne svrhe, te ako na drugom mjestu na ovoj veb lokaciji nije drugačije
                    naznačeno. Ipak, sadržaj ove stranice ne možete na bilo koji način mijenjati,
                    niti ga reprodukovati ili javno prikazivati, distribuirati, izvoditi, ili
                    koristiti na bilo koji drugi način u komercijalne ili privatne svrhe. Svaki
                    softver ili drugi materijal dostupan za preuljetonje ili upotrebu putem ove
                    stranice koji ima svoje uslove licenciranja ili korišćenja, zaštićen je tim
                    uslovima. U slučaju da prekršite bilo koji od ovih uslova, vaše odobrenje za
                    korišćenje sajta biće opozvano i bićete dužni da odmah uništite sav sadržaj koji
                    ste preuzeli ili odštampali. Ako ovdje nije izričito navedeno ili na bilo kojoj
                    drugoj stranici ovog veb sajta, ništa što je ovdje sadržano, bilo izričito,
                    podrazumijevano, odricanjem odgovornosti ili na neki drugi način, ne daje vam
                    pravo da bilo koji dio intelektualne svojine koji se nalazi na veb stranici
                    koristite.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>INFORMACIJE</h3>

                  <p>
                    Svi materijali, povratne informacije ili druge vrste informacija koje dostavite
                    ovom sajtu ili u našoj kompaniji povodom ovog sajta neće biti smatrani za
                    povjerljive ili vlasničke. Tudors nema obavezu u vezi sa takvim informacijama i
                    slobodan je da ih bez ograničenja koristi u bilo koju svrhu i bez pozivanja na
                    izvore. Postavljanje ili prenošenje na sajt ili sa njega bilo kog nezakonitog,
                    prijetećeg, klevetničkog, opscenog, pornografskog, ponižavajućeg illi materijala
                    koji krši bilo koji zakon je zabranjeno.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>PRIVATNOST</h3>

                  <p>
                    Tudors zadržava pravo na prikupljanje određenih ličnih podataka prilikom
                    pretraživanja nekih veb lokacija ove veb stranice kako bi udovoljio vašim
                    zahtjevima ili potrebama. Svako prikupljanje takvih podataka na ovoj veb
                    lokaciji odvijaće se u skladu sa politikom privatnosti sadržanom na ovoj veb
                    stranici, kao i važećim propisima o zaštiti ličnih podataka. Neke stranice ovog
                    veb sajta mogu na vašem računaru da čuvaju određene pakete podataka poznate kao
                    „kolačići“. Oni nam daju odgovore na pitanja kada i na koji način je ova
                    stranica posjećena i koliko ljudi ju je posetilo. Ova tehnologija ne prikuplja
                    informacije o vašem ličnom identitetu, već su informacije u zbirnom obliku. Cilj
                    ove tehnologije i informacija koje se prikupljaju je unaprijeđenje našeg veb
                    sajta.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>OGRANIČENJE GARANCIJA</h3>

                  <p>
                    Sadržaji prikazani na ovoj veb stranici prikazuju se „kao takvi“ bez ikakvih
                    jamstava. Tudors je izričito ograničen na maksimalnu mjeru određenu zakonom, iz
                    bilo kojeg direktne, indirektne zakonske ili bilo koje druge garancije ili
                    predstavljanja, uključujući jamstva tržišne profitabilnosti, pogodnosti za
                    određenu svrhu ili poštovanja intelektualnih ili vlasničkih prava. Sadržaj se
                    sastoji isključivo od informacija opšte prirode koje nisu namijenjene rješavanju
                    određenih situacija ili za određenu osobu ili identitet i koje ne predstavljaju
                    profesionalni savjet. U maksimalnoj mjeri dozvoljenoj zakonom, Tudors ne
                    garantuje pouzdanost, tačnost, primjenljivost i kompletnost sadržaja na ovoj veb
                    lokaciji, kao ni garanciju nesmetanog rada, blagovremenosti, sigurnosti ili
                    izostanka grešaka. Tudors u bilo koje vrijeme i bez najave može izmijeniti
                    sadržaj ove stranice ili proizvode i cijene istaknute na njoj. Tudors nije u
                    obavezi da ažurira sadržaj na ovom veb sajtu, te on može biti zastareo.
                    Proizvodi, usluge ili programi sadržani na ovom sajtu ne moraju biti dostupni u
                    vašoj zemlji. Posavjetujte se sa lokalnom kancelarijom Tudors u vezi sa
                    proizvodima, programima ili uslugama koji su vam dostupni.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>OGRANIČENJE ODGOVORNOSTI</h3>

                  <p>
                    Tudors, njegovi dobavljači ili treće strane pomenuti na ovoj veb lokaciji, u
                    maksimalnoj mjeri dozvoljenoj zakonom, ni u kom slučaju neće biti odgovorni za
                    bilo kakvu štetu (bez obzira da li je direktna, indirektnu, posebna,
                    posljedična, slučajna šteta ili kao rezultat gubitka, podataka, profila ili
                    prekida poslovnog procesa) koji je rezultat upotrebe, kašnjenja ili nemogućnosti
                    korišćenja ili rezultata upotrebe ove veb lokacije, veb sajtova povezanih sa ove
                    stranice ili sadržaja objavljenog ovdje ili na drugim pomenutim veb sajtovima,
                    bez obzira da li se zasniva na garanciji, ugovoru ili drugom pravnom sredstvu i
                    bez obzira da li je klijent obavješten o mogućnosti takve štete ili ne. Ako je
                    korišćenje usluga, informacija ili materijala dovelo do potrebe za servisiranjem
                    ili popravkom opreme ili podataka, odgovorni ste za nastale troškove.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>REGISTROVANI ŽIGOVI</h3>

                  <p>
                    Sva imena, žigovi, logotipi ili zaštitni znakovi usluga koji se pojavljuju na
                    ovoj veb lokaciji su registrovani zaštitni znaci određenih kompanija u okviru
                    Tudors-a ili drugih vlasnika. Bez njihovog prethodnog pristanka, ne možete
                    koristiti imena, žigove, logotipe ili zaštitne znakove. Tudors maksimalno
                    koristi sve svoje resurse kako bi vam prikazao sve stavke na ovoj veb stranici
                    sa tačnim nazivima specifikacija, fotografija i cijena. Međutim, ne možemo
                    garantovati da su sve informacije i fotografije predmeta na ovoj veb lokaciji u
                    potpunosti tačni.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>CIJENE</h3>

                  <p>
                    Cijene su izražene u eurima sa PDV-om. Tudors zadržava pravo promjene cijena bez
                    prethodne najave. Stanje zaliha se ažurira nekoliko puta tokom dana. Trudimo se
                    da što je moguće preciznije opisujemo sve proizvode, ali ne možemo u potpunosti
                    garantovati da su svi opisi potpuni i bez grešaka. Tudors ima pravo da otkaže
                    porudžbinu slanjem obavještenja potrošaču putem e-pošte ako je došlo do grube
                    greške prilikom postavljanja cijene na veb sajtu. Hvala na razumijevanju.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>ISPORUKA</h3>

                  <p>
                    Organizovali smo kućnu dostavu za sve proizvode naručene putem našeg internet
                    sajta sa rokom isporuke od pet* dana od dana poručivanja. Trošak dostave je 3
                    eura* za sve gradove u Crnoj Gori.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>PLAĆANJE</h3>

                  <p>
                    Uplatu u internet prodavnici možete izvršiti platnom karticom (Visa, MasterCard,
                    Maestro) ili pouzećem. Prilikom procesa naručivanja platnom karticom, automatski
                    ćete biti preusmjereni na veb sajt CKB banke koja obrađuje kartice putem
                    Interneta. Nakon unosa podataka, banka provjerava vašu karticu i raspoloživost
                    sredstava na računu. O statusu plaćanja bićete obaviješteni e-poštom. Plaćanje
                    pouzećem vršite u trenutku isporuke proizvoda. Prije nego što vam se isporuče
                    proizvodi, kontaktiraće vas Tudors da potvrdi porudžbinu.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>ZAŠTITA KORISNIČKE PRIVATNOSTI</h3>

                  <p>
                    U ime Tudors-a, zalažemo se za zaštitu privatnosti svih naših kupaca.
                    Prikupljamo samo potrebne osnovne podatke o kupcima/korisnicima i podatke
                    potrebne za poslovanje i informisanje korisnika u cilju pružanja kvalitetne
                    usluge i u skladu sa dobrom poslovnom praksom. Kupcima dajemo mogućnost izbora,
                    kao i mogućnost da odluče da li žele da budu izbrisani sa mailing lista koje se
                    koriste za marketinške kampanje. Svi podaci o korisnicima/kupcima strogo se
                    čuvaju i dostupni su samo zaposlenima kojima su ti podaci potrebni za obavljanje
                    posla. Svi zaposleni u Tudors-u odgovorni su za poštovanje principa zaštite
                    privatnosti.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>POVRAĆAJ ROBE</h3>

                  <p>
                    U slučaju povrata robe i vraćanja sredstava kupcu koji je prethodno platio
                    jednom od platnih kartica, djelimično ili u cjelosti, i bez obzira na razlog
                    vraćanja, Tudors je dužan da vrati sredstva samo putem VISA, EC/MC i Maestro
                    načina plaćanja. To znači da će banka, na zahtjev prodavca, vratiti novac na
                    račun vlasnika kartice.
                  </p>
                </div>

                <div className='text-container'>
                  <h3>ZAŠTITA POVJERLJIVIH PODATAKA O TRANSAKCIJI</h3>

                  <p>
                    Povjerljivi podaci se, prilikom unosa podataka platnih kartica, prenose putem
                    javne mreže u zaštićenom (šifrovanom) obliku pomoću SSL protokola i PKI sistema,
                    kao trenutno najsavremenije kriptografske tehnologije. Procesor platnih kartica
                    garantuje sigurnost podataka tokom kupovine, tako da se kompletan postupak
                    obračuna vrši na veb sajtu banke. Informacije o platnim karticama nisu dostupne
                    našem sistemu ni u jednom trenutku.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default UsloviKupovine;
