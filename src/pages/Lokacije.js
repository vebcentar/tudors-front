import React, { useEffect } from "react";
import { Helmet } from "react-helmet";

const Lokacije = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div>
      {/*<Helmet>
        <title>Tudors | Lokacije</title>
      </Helmet>*/}
      <div
        className='intro-banner'
        style={{
          backgroundImage: `url(${require("../img/lokacije/intro-banner.png")})`,
        }}></div>

      <main>
        <div className='lokacije-section'>
          <div className='container pt-50 pb-80'>
            <div className='row'>
              <div className='col-12'>
                <div className='title'>
                  <h1>Lokacije</h1>
                </div>

                <div className='line'></div>
              </div>
            </div>

            <div className='row'>
              <div className='col-lg-5 col-md-4 c-col'>
                <div className='adresa'>
                  <div className='title'>
                    <p>Adresa:</p>
                  </div>
                  <a
                    href='https://goo.gl/maps/HSsVEoJxQHh6eYHV6'
                    target='_blank'
                    rel='noreferrer noopener'>
                    Ulica Slobode 76, 81000 Podgorica
                  </a>
                </div>
              </div>

              <div className='col-lg-2 col-md-3 c-col'>
                <div className='tel'>
                  <div className='title'>
                    <p>Broj telefona:</p>
                  </div>
                  <a href='tel: +38267675414' target='_blank' rel='noreferrer noopener'>
                    +382 67 675 414
                  </a>
                </div>
              </div>

              <div className='col-lg-5 col-md-5 c-col'>
                <div className='radno-vrijeme'>
                  <div className='title'>
                    <p>Radno vrijeme:</p>
                  </div>
                  <div className='text'>
                    <span>Ponedeljak - Subota 10:00 - 22:00 ; </span>
                    <span>Nedeljom ne radimo</span>
                  </div>
                </div>
              </div>
            </div>

            <div className='h-line'></div>

            <div className='row'>
              <div className='col-lg-5 col-md-4 c-col'>
                <div className='adresa'>
                  <div className='title'>
                    <p>Adresa:</p>
                  </div>
                  <a
                    href='https://goo.gl/maps/HSsVEoJxQHh6eYHV6'
                    target='_blank'
                    rel='noreferrer noopener'>
                    Ulica Slobode 76, 81000 Podgorica
                  </a>
                </div>
              </div>

              <div className='col-lg-2 col-md-3 c-col'>
                <div className='tel'>
                  <div className='title'>
                    <p>Broj telefona:</p>
                  </div>
                  <a href='tel: +38267675414' target='_blank' rel='noreferrer noopener'>
                    +382 67 675 414
                  </a>
                </div>
              </div>

              <div className='col-lg-5 col-md-5 c-col'>
                <div className='radno-vrijeme'>
                  <div className='title'>
                    <p>Radno vrijeme:</p>
                  </div>
                  <div className='text'>
                    <span>Ponedeljak - Subota 10:00 - 22:00 ; </span>
                    <span>Nedeljom ne radimo</span>
                  </div>
                </div>
              </div>
            </div>

            <div className='h-line'></div>

            <div className='row'>
              <div className='col-lg-5 col-md-4 c-col'>
                <div className='adresa'>
                  <div className='title'>
                    <p>Adresa:</p>
                  </div>
                  <a
                    href='https://goo.gl/maps/HSsVEoJxQHh6eYHV6'
                    target='_blank'
                    rel='noreferrer noopener'>
                    Ulica Slobode 76, 81000 Podgorica
                  </a>
                </div>
              </div>

              <div className='col-lg-2 col-md-3 c-col'>
                <div className='tel'>
                  <div className='title'>
                    <p>Broj telefona:</p>
                  </div>
                  <a href='tel: +38267675414' target='_blank' rel='noreferrer noopener'>
                    +382 67 675 414
                  </a>
                </div>
              </div>

              <div className='col-lg-5 col-md-5 c-col'>
                <div className='radno-vrijeme'>
                  <div className='title'>
                    <p>Radno vrijeme:</p>
                  </div>
                  <div className='text'>
                    <span>Ponedeljak - Subota 10:00 - 22:00 ; </span>
                    <span>Nedeljom ne radimo</span>
                  </div>
                </div>
              </div>
            </div>

            <div className='h-line'></div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Lokacije;
