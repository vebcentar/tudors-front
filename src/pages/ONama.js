import React,{useEffect} from "react";
import mainImg from "../img/o-nama/mainImg.png";
import { Helmet } from "react-helmet";

const ONama = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div>
         {/*<Helmet>
        <title>Tudors | O nama</title>
         </Helmet>*/}
      <div
        className="intro-banner"
        style={{
          backgroundImage: `url(${require("../img/o-nama/intro-banner.png")})`,
        }}
      ></div>
      <main>
        <div className="o-nama-section">
          <div className="container pt-50 pb-80">
            <div className="row">
              <div className="col-12">
                <div className="title">
                  <h1>O nama</h1>
                </div>

                <div className="line"></div>

                <div className="text-container">
                  <p>
                    Košulja je najčešći odevni komad koji muškarci biraju još od
                    davnina i prikladan izgled muškarca danas se ne može se
                    zamisliti bez dobre košulje. Tudors je brend namenjen
                    isključivo muškarcima, lider u proizvodnji modernih košulja
                    namenjenih muškarcima svih godina, zanimanja i modnih
                    pravaca. Tudors nudi moderne i elegantne, strukirane i
                    klasične, poslovne i sportske muške košulje u svim bojama i
                    dezenima. Ono što će najviše obradovati sve potencijalne
                    kupce TUDORS brenda je to da su cene više nego pristupačne.
                    <br />
                    <br />
                    Tudors je nastao u Turskoj od male porodične firme za
                    proizvodnju košulja. Sinovi su nastavili tradiciju koju je
                    započeo njihov otac, napravili su brend Tudors koji nudi
                    veliki izbor košulja po veoma pristupačnim cenama. Za samo
                    nekoliko godina brend je postigao veliki uspeh tako da sada
                    broji preko 230 radnji u svetu.
                    <br />
                    <br />
                    Pored velikog izbora košulja, Tudors u ponudi ima polo
                    majice, džempere, prsluke, rolke i veliki izbor aksesoara -
                    kravate, leptir mašne, tregere, kaiševe, dugmad za manžetne,
                    donji veš i mnogi drugi. Skoro da je nemoguće zatražiti boju
                    ili model košulje koju Tudors nema u šta možete i sami da se
                    uverite u našoj online prodavnici!
                  </p>
                </div>

                <div className="img-container">
                  <img src={mainImg} alt="" />
                </div>
              </div>
            </div>
          </div>

          <div className="bottom-bg"></div>
        </div>
      </main>
    </div>
  );
};

export default ONama;
