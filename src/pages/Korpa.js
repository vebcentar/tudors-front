import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import { ProizvodKorpa } from "../components/Proizvod/ProizvodKorpa";
import ProductsContext from "../context/productsContext/productstContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";

// url(./img/korpa/intro-banner.png)
const Korpa = () => {
  const { cart, removeFromCart, addToCart, decraseAmount } = useContext(ProductsContext);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const totalPrice = cart.reduce(function (earnings, item) {
    console.log(item);
    return earnings + +item.price * item.amount;
  }, 0);

  const notify = (name) => toast(name);

  function remove(item) {
    removeFromCart(item);
    notify("Uklonjen proizvod" + item.name);
  }

  return (
    <>
      {/*<Helmet>
        <title>Tudors | Korpa</title>
      </Helmet>*/}
      <ToastContainer autoClose={1000} hideProgressBar />

      <div
        className='intro-banner'
        style={{
          backgroundImage: `url(${require("../img/korpa/intro-banner.png")})`,
        }}></div>
      <main>
        <div className='korpa-section'>
          <div className='container pt-50 pb-80'>
            <div className='row'>
              <div className='col-12'>
                <div className='title'>
                  <h1>Korpa</h1>
                </div>

                <div className='red-line'></div>
              </div>
            </div>

            {cart.length === 0 ? (
              <div className='title'>
                <h2>Trenutno nema proizvoda u korpi</h2>
              </div>
            ) : (
              <>
                {cart.length > 0 &&
                  cart.map((item) => {
                    return (
                      <ProizvodKorpa
                        key={item.size}
                        name={item.name}
                        code={item.product_code}
                        price={item.price}
                        size={item.size}
                        itemAmount={item.amount}
                        image={item.image}
                        remove={() => remove(item)}
                        add={() => addToCart(item)}
                        decrase={() => decraseAmount(item)}
                      />
                    );
                  })}
                <div className='row'>
                  <div className='col-12'>
                    <div className='ukupna-cijena'>
                      <div className='ukupno'>
                        Ukupno:
                        <span className='cijena'>{totalPrice.toFixed(2)} €</span>
                      </div>
                      <div className='dostava'>
                        Cijena dostave pouzećem:
                        <span className='cijena'>3.00€</span>
                      </div>
                      <div className='isporuka'>
                        <span>Isporuku vršimo samo na teritoriji Crne Gore.</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div className='row row-ch mt-30'>
                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <div className='nazad'>
                      <Link to='./proizvodi'>
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 24 24'
                          fill='#58585a'
                          width='24px'
                          height='24px'>
                          <path d='M0 0h24v24H0V0z' fill='none' />
                          <path d='M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z' />
                        </svg>
                        <span>NAZAD</span>
                      </Link>
                    </div>
                  </div>

                  <div className='col-lg-6 col-md-6 col-sm-6'>
                    <Link to='/isporuka'>
                      <div className='nastavite'>
                        <button className='tr-3'>Nastavite</button>
                      </div>
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </main>
    </>
  );
};

export default Korpa;
