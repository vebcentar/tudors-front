import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";

const Success = () => {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem("confirmRegistration")) {
      history.push("/error");
    }
    localStorage.setItem("confirmRegistration", true);
  }, [history]);
  return (
    <div style={{ minHeight: "calc(100vh - 250px)" }}>
      {/*<Helmet>
        <title>Tudors | Uspešna potvrda</title>
      </Helmet>*/}
      <div
        className='intro-banner '
        style={{
          backgroundImage: `url(${require("../img/proizvodi/intro-banner.png")})`,
        }}></div>
      <div className='d-flex align-items-center flex-column'>
        <h2 className='text-center mt-5 mb-5'>Uspešno ste se registrovali. Dobro došli!</h2>

        <button
          style={{
            width: 300,
            height: "5rem",
            border: "2px solid #71041b",
            color: "#71041b",
            fontSize: "1.6rem",
          }}
          className='mt-5'
          onClick={() => history.push("/")}>
          Početna
        </button>
      </div>
    </div>
  );
};

export default Success;
