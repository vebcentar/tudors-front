import React, { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { useLocation, useParams } from "react-router-dom";
import axios from "axios";
import ProductsContext from "../context/productsContext/productstContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";
import OwlCarousel from "react-owl-carousel";
import FloatingCart from "../components/FloatingCart/FloatingCart";

const url = `${process.env.REACT_APP_TUDORS_API}api/products/getProduct`;
const urlSpec = `${process.env.REACT_APP_TUDORS_API}`;

const ProizvodDetalji = () => {
  const [productImage, setProductImages] = useState([]);
  const [onStock, setOnStock] = useState(0);
  const [ammount, setAmmount] = useState(1);
  const [alertDisplay, setAlertDisplay] = useState("none");
  const [size, setSize] = useState(null);
  const { id } = useParams();
  const [proizvod, setProizvod] = useState();

  const [prodCategory, setProdCategory] = useState("");
  const [breadArr, setBreadArr] = useState(null);
  const [floatingCart, setFloatingCart] = useState(false);

  const {
    addToCart,
    specialProducts,
    loadSpecialProducts,
    categories,
  } = useContext(ProductsContext);
  useEffect(() => {
    if (floatingCart) {
      document.documentElement.style.overflow = "hidden";
      return () => (document.documentElement.style.overflow = "unset");
    }
  }, [floatingCart]);
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    if (id) {
      fetchProizvod(id);
    }
  }, [id]);

  useEffect(() => {
    if (proizvod) {
      const numberOfProduct = proizvod.sizes.reduce(
        (acc, cur) => acc + cur.amount,
        0
      );
      setOnStock(numberOfProduct);
      createBreadCrump(proizvod);
    }
  }, [proizvod]);

  useEffect(() => {
    console.log("SVI PROIZVODI");
    console.log(proizvod);
  });
  const fetchProizvod = async (id) => {
    try {
      const res = await axios.post(url, { id: id });
      setProizvod(res.data);
      setProductImages(res.data.images);
      // createBreadCrump(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  const notify = (name) => toast("Dodato u korpu:" + name);

  const add = () => {
    if (size) {
      addToCart({
        ...proizvod.product,
        amount: ammount,
        size: size,
        sizeId: proizvod.sizes.filter((item) => item.name === size)[0]
          .product_size_id,
        image: productImage.length ? productImage[0].image : null,
      });
      // notify(proizvod.product.name);
      setFloatingCart(true);
    } else {
      setAlertDisplay("flex");
    }
  };

  useEffect(() => {
    const buttons = document.getElementsByClassName("sizeBtn");
    console.log(buttons.length);
    if (buttons.length) {
      for (let btn of buttons) {
        btn.classList.remove("sizesBtnBorder");
        if (btn.name === size) {
          btn.classList.add("sizesBtnBorder");
        }
      }
    }
    // sizesBtnBorder
  }, [size]);

  const fetchProducts = async () => {
    try {
      const res = await axios.post(`${urlSpec}api/products/specialOffers`, {});
      await loadSpecialProducts(res.data.products);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  const createBreadCrump = (prod) => {
    const prodId = prod.product.category_id;
    const lozaArr = [];

    categories.forEach((cat) => {
      if (cat.id === prodId) {
        lozaArr.push(cat);
      } else if (cat.children) {
        cat.children.forEach((chilCat) => {
          if (chilCat.id === prodId) {
            lozaArr.push(cat);
            lozaArr.push(chilCat);
          }
        });
      }
    });
    setProdCategory(lozaArr[lozaArr.length - 1].name);
    setBreadArr(lozaArr);
    // console.log(lozaArr);
  };

  return proizvod ? (
    <>
      {/*<Helmet>
        <title>Tudors | {proizvod.product.name}</title>
      </Helmet>*/}
      <ToastContainer autoClose={1000} hideProgressBar />
      <div
        className="intro-banner"
        style={{
          backgroundImage: `url(${require("../img/proizvod-detalji/intro-banner.png")})`,
        }}
      ></div>
      <main>
        <div className="proizvod-detalji-section">
          <div className="container pt-50 pb-80">
            <div className="row">
              <div className="col-12">
                <div className="title">
                  <h1>{prodCategory}</h1>
                  <div className="category">
                    {breadArr && (
                      <>
                        <Link to="/">Početna</Link>
                        {breadArr.map((item) => (
                          <Link
                            key={item.id}
                            to={{
                              pathname: `/proizvodi/${item.slug}`,
                              state: { id: item.id, name: item.name },
                              search: `id=${item.id}&name=${item.name}`,
                            }}
                          >{`/${item.name}`}</Link>
                        ))}
                      </>
                    )}
                    {/* <span className="divider"> /</span> */}
                    {/* <Link to="/proizvod-detalji">Slim fit</Link> */}
                    {/* <span className="divider"> / </span> */}
                    {/* <Link to="/proizvod-detalji">Dugi rukav</Link> */}
                  </div>
                </div>

                <div className="red-line"></div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-6">
                <div className="left-side">
                  {/* <div
                    id="carousel-proizvod-detalji"
                    className="owl-carousel owl-theme"
                  >
                    <div
                      className="item carousel-img"
                      style={{
                        backgroundImage: `url(${require("../img/proizvod-detalji/carousel-img-1.png")})`,
                      }}
                    ></div>

                    <div
                      className="item carousel-img"
                      style={{
                        backgroundImage: `url(${require("../img/proizvod-detalji/carousel-img-1.png")})`,
                      }}
                    ></div>
                    <div
                      className="item carousel-img"
                      style={{
                        backgroundImage: `url(${require("../img/proizvod-detalji/carousel-img-1.png")})`,
                      }}
                    ></div>
                  </div> */}

                  {productImage.length && (
                    <OwlCarousel
                      id="carousel-proizvod-detalji"
                      items={1}
                      className="owl-carousel owl-theme"
                      loop
                      margin={10}
                      nav
                      dots={false}
                      navText={[
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                      ]}
                    >
                      {productImage.map((img, idx) => {
                        return (
                          <div
                            key={idx}
                            className="item carousel-img"
                            style={{
                              backgroundImage: `url(https://admin.tudorsshop.me/${img.image})`,
                            }}
                          ></div>
                        );
                      })}
                    </OwlCarousel>
                  )}
                </div>
              </div>

              <div className="col-lg-6">
                <h3 style={{ display: alertDisplay }} className="size-alert">
                  MOLIM VAS ODABERITE VELIČINU I POKUŠAJTE PONOVO
                </h3>
                <div className="right-side">
                  <div className="naslov">
                    <h3>{proizvod.product.name}</h3>
                  </div>

                  <div className="sifra">
                    Šifra proizvoda:{" "}
                    <span>{proizvod.product.product_code}</span>
                  </div>

                  <div className="line my-30"></div>

                  <div className="row row-cijena-kolicina">
                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                      <div className="cijena">
                        <span>
                          {proizvod.product.discount_value
                            ? (
                                (1 - proizvod.product.discount_value / 100) *
                                proizvod.product.price
                              ).toFixed(2)
                            : proizvod.product.price}
                          €
                        </span>{" "}
                        <span
                          style={{
                            textDecoration: "line-through",
                            fontSize: "2rem",
                            color: "black",
                            fontWeight: 500,
                          }}
                          className="ml-5"
                        >
                          {proizvod.product.discount_value
                            ? proizvod.product.price
                            : null}
                          €
                        </span>
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-6 col-sm-6 col-6">
                      <div className="kolicina-container">
                        <div className="kolicina-text">
                          <span>Količina:</span>
                        </div>
                        <div className="kolicina d-flex align-items-center">
                          <button
                            onClick={() =>
                              setAmmount((prev) => {
                                if (prev > 1) {
                                  return prev - 1;
                                } else {
                                  return prev;
                                }
                              })
                            }
                            className="icon-container d-flex justify-content-center align-items-center"
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              fill="black"
                              width="18px"
                              height="18px"
                            >
                              <path d="M0 0h24v24H0V0z" fill="none" />
                              <path d="M19 13H5v-2h14v2z" />
                            </svg>
                          </button>
                          <input
                            id="cart_amount_input"
                            className="cart_amount_input"
                            min="1"
                            type="number"
                            value={ammount}
                            onChange={() => {}}
                          />
                          <button
                            onClick={() => setAmmount((prev) => prev + 1)}
                            className="icon-container d-flex justify-content-center align-items-center"
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 24 24"
                              fill="black"
                              width="18px"
                              height="18px"
                            >
                              <path d="M0 0h24v24H0V0z" fill="none" />
                              <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z" />
                            </svg>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="line my-30"></div>

                  <div className="row row-odaberite-velicinu">
                    <div className="col-lg-7 col-md-7 col-sm-7">
                      <div className="velicine">
                        <label>Odaberite veličinu:</label>
                        <div>
                          {onStock === 0 ? (
                            <h3>Proizvod nije dostupan</h3>
                          ) : (
                            <>
                              {" "}
                              {proizvod.sizes.map((item) => (
                                <button
                                  className="sizeBtn"
                                  key={item.name}
                                  name={item.name}
                                  disabled={item.amount === 0 ? true : false}
                                  onClick={() => {
                                    setSize(item.name);
                                  }}
                                >
                                  {item.name}
                                </button>
                              ))}
                            </>
                          )}
                        </div>
                      </div>
                    </div>

                    {/* <div className="col-lg-5 col-md-5 col-sm-5">
                      <div className="tabela">
                        <Link to="proizvod-detalji">Tabela veličina</Link>
                        <Link to="proizvod-detalji">
                          Dostupno u prodavnicama!
                        </Link>
                      </div>
                    </div> */}
                  </div>

                  <div className="row row-3 my-40">
                    <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="korpa-button">
                        <button onClick={add}>Dodajte u korpu</button>
                      </div>
                      {floatingCart && (
                        <FloatingCart
                          show={floatingCart}
                          close={() => setFloatingCart(false)}
                        />
                      )}
                    </div>

                    {/* <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="podijelite">
                        <label>Podijelite na:</label>

                        <div className="social">
                          <Link to="" target="_blank" rel="noreferrer noopener">
                            <div className="icon-container tr-3">
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="fab"
                                data-icon="facebook-f"
                                className="svg-inline--fa fa-facebook-f fa-w-10"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 320 512"
                                height="20px"
                                width="20px"
                              >
                                <path
                                  fill="white"
                                  d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"
                                ></path>
                              </svg>
                            </div>
                          </Link>

                          <Link to="" target="_blank" rel="noreferrer noopener">
                            <div className="icon-container tr-3">
                              <svg
                                aria-hidden="true"
                                focusable="false"
                                data-prefix="fab"
                                data-icon="twitter"
                                className="svg-inline--fa fa-twitter fa-w-16"
                                role="img"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512"
                                height="20px"
                                width="20px"
                              >
                                <path
                                  fill="white"
                                  d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                                ></path>
                              </svg>
                            </div>
                          </Link>
                        </div>
                      </div>
                    </div> */}
                  </div>

                  <div className="desc">
                    <ul>
                      <li>
                        BOJA : <span>{proizvod.product.color}</span>
                      </li>
                      {/* <li>
                        MODEL: <span>Slim Fit</span>
                      </li>
                      <li>
                        TIP RUKAVA : <span>Dugi Rukavi</span>
                      </li>
                      <li>
                        ŠARA : <span>Sa Šarom</span>
                      </li>
                      <li>
                        IZREZ VRATA : <span>Sa Kragnom</span>
                      </li>
                      <li>
                        SASTAV : <span>Shirt Men's (65% Polyester 35% Cotton)</span>
                      </li> */}
                    </ul>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-12">
                <div className="section-divider my-50"></div>
              </div>
            </div>

            <div className="izdvajamo-iz-ponude-section">
              <div className="container">
                <div className="row">
                  <div className="col-12">
                    <div className="section-title">
                      <h2>
                        Izdvajamo <span>iz ponude</span>
                      </h2>
                    </div>
                  </div>
                </div>

                {/* <div id='carousel-izdvajamo-iz-ponude' className='owl-carousel owl-theme'> */}
                {/* <div> */}
                {specialProducts.length ? (
                  <OwlCarousel
                    id="carousel-izdvajamo-iz-ponude"
                    items={specialProducts.length - 1}
                    className="owl-carousel owl-theme d-flex flex-column"
                    loop={true}
                    margin={10}
                    nav={true}
                    center={true}
                    dots={false}
                    responsive={{
                      0: {
                        items: 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                      374: {
                        items:
                          specialProducts.length > 2
                            ? 2
                            : specialProducts.length - 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                      576: {
                        items:
                          specialProducts.length > 2
                            ? 2
                            : specialProducts.length - 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                      660: {
                        items:
                          specialProducts.length > 3
                            ? 3
                            : specialProducts.length - 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                      860: {
                        items:
                          specialProducts.length > 4
                            ? 4
                            : specialProducts.length - 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                      991: {
                        items:
                          specialProducts.length > 5
                            ? 5
                            : specialProducts.length - 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                      1400: {
                        items:
                          specialProducts.length > 5
                            ? 5
                            : specialProducts.length - 1,
                        nav: true,
                        dots: false,
                        navText: [
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="36px" height="36px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                        ],
                      },
                    }}
                    navText={[
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"/></svg>',
                      '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="22px" height="22px"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/></svg>',
                    ]}
                  >
                    {specialProducts.map((data, idx) => {
                      return (
                        <div
                          key={idx}
                          className="item   mx-auto"
                          style={{ maxWidth: "200px" }}
                        >
                          <Link to={`/proizvod-detalji/${data.id}`}>
                            <div className="content-container">
                              <div className="top-line"></div>

                              <div
                                className="img-container"
                                style={{
                                  backgroundImage: `url(https://admin.tudorsshop.me/${data.image})`,
                                }}
                              ></div>

                              <div className="text-container">
                                <div className="proizvod">
                                  <span>{data.name}</span>
                                </div>

                                <div className="cijena-proizvoda">
                                  <span className="cijena">
                                    {proizvod.product.discount_value
                                      ? (
                                          (1 - data.discount_value / 100) *
                                          data.price
                                        ).toFixed(2)
                                      : data.price}
                                    €
                                  </span>
                                  <span className="separator">
                                    {data.special_offer > 0 ? " / " : null}
                                  </span>
                                  <span className="popust">
                                    {data.discount_value ? data.price : null}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </Link>
                        </div>
                      );
                    })}
                    {/* <div className='item'>
                  <Link to=''>
                    <div className='content-container'>
                      <div className='top-line'></div>

                      <div
                        className='img-container'
                        style={{
                          backgroundImage: `url(${require("../img/naslovna/ponuda-img-1.png")})`,
                        }}></div>

                      <div className='text-container'>
                        <div className='proizvod'>
                          <span>Košulja, slim fit,</span>
                          <span>dugi rukav</span>
                        </div>

                        <div className='cijena-proizvoda'>
                          <span className='cijena'>28.00€</span>
                          <span className='separator'> / </span>
                          <span className='popust'>35.00€</span>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div> */}
                  </OwlCarousel>
                ) : (
                  <p>Ucitavanje...</p>
                )}

                {/* 
              <div className='item'>
                <Link to=''>
                  <div className='content-container'>
                    <div className='top-line'></div>

                    <div
                      className='img-container'
                      style={{
                        backgroundImage: `url(${require("../img/naslovna/ponuda-img-1.png")})`,
                      }}></div>

                    <div className='text-container'>
                      <div className='proizvod'>
                        <span>Košulja, slim fit,</span>
                        <span>dugi rukav</span>
                      </div>

                      <div className='cijena-proizvoda'>
                        <span className='cijena'>28.00€</span>
                        <span className='separator'> / </span>
                        <span className='popust'>35.00€</span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div> */}

                {/* <div className='item'>
                <Link to=''>
                  <div className='content-container'>
                    <div className='top-line'></div>

                    <div
                      className='img-container'
                      style={{
                        backgroundImage: `url(${require("../img/naslovna/ponuda-img-2.png")})`,
                      }}></div>

                    <div className='text-container'>
                      <div className='proizvod'>
                        <span>Majica, okrugli izrez,</span>
                        <span>sa teksturom</span>
                      </div>

                      <div className='cijena-proizvoda'>
                        <span className='cijena'>15.00€</span>
                        <span className='separator'></span>
                        <span className='popust'></span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div> */}

                {/* <div className='item'>
                <Link to=''>
                  <div className='content-container'>
                    <div className='top-line'></div>

                    <div
                      className='img-container'
                      style={{
                        backgroundImage: `url(${require("../img/naslovna/ponuda-img-3.png")})`,
                      }}></div>

                    <div className='text-container'>
                      <div className='proizvod'>
                        <span>Košulja, klasik fit,</span>
                        <span>dugi rukav</span>
                      </div>

                      <div className='cijena-proizvoda'>
                        <span className='cijena'>22.00€</span>
                        <span className='separator'></span>
                        <span className='popust'></span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div> */}

                {/* <div className='item'>
                <Link to=''>
                  <div className='content-container'>
                    <div className='top-line'></div>

                    <div
                      className='img-container'
                      style={{
                        backgroundImage: `url(${require("../img/naslovna/ponuda-img-4.png")})`,
                      }}></div>

                    <div className='text-container'>
                      <div className='proizvod'>
                        <span>Džemper, slim fit,</span>
                        <span>v izrez</span>
                      </div>

                      <div className='cijena-proizvoda'>
                        <span className='cijena'>19.00€</span>
                        <span className='separator'></span>
                        <span className='popust'></span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div> */}

                {/* <div className='item'>
                <Link to=''>
                  <div className='content-container'>
                    <div className='top-line'></div>

                    <div
                      className='img-container'
                      style={{
                        backgroundImage: `url(${require("../img/naslovna/ponuda-img-5.png")})`,
                      }}></div>

                    <div className='text-container'>
                      <div className='proizvod'>
                        <span>Košulja, slim fit,</span>
                        <span>dugi rukav</span>
                      </div>

                      <div className='cijena-proizvoda'>
                        <span className='cijena'>28.00€</span>
                        <span className='separator'> / </span>
                        <span className='popust'>35.00€</span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div> */}

                {/* <div className='item'>
                <Link to=''>
                  <div className='content-container'>
                    <div className='top-line'></div>

                    <div
                      className='img-container'
                      style={{
                        backgroundImage: `url(${require("../img/naslovna/ponuda-img-1.png")})`,
                      }}></div>

                    <div className='text-container'>
                      <div className='proizvod'>
                        <span>Košulja, slim fit,</span>
                        <span>dugi rukav</span>
                      </div>

                      <div className='cijena-proizvoda'>
                        <span className='cijena'>28.00€</span>
                        <span className='separator'> / </span>
                        <span className='popust'>35.00€</span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div> */}
                {/* </div> */}
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  ) : (
    <p>Ucitavanje</p>
  );
};

export default ProizvodDetalji;
