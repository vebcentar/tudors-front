import React, { Suspense, lazy, useEffect } from "react";

import { BrowserRouter as Router, Switch } from "react-router-dom";

import "./styles/styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/animate.css";

import Isporuka from "./pages/Isporuka";
import Kontakt from "./pages/Kontakt";
import Korpa from "./pages/Korpa";
import Lokacije from "./pages/Lokacije";
import ONama from "./pages/ONama";
import ProizvodDetalji from "./pages/ProizvodDetalji";
import Proizvodi from "./pages/Proizvodi";
import UsloviKupovine from "./pages/UsloviKupovine";
import DefaultLayout from "./components/DefaultLayout";
import ProductState from "./context/productsContext/ProductsState";
import AuthState from "./context/authContext/AuthState";
import SuccessOrder from "./pages/SuccessOrder";
import SplashScreen from "./components/SplashScreen/SplashScreen";
import Success from "./pages/Success";
import Error from "./pages/Error";
import ConfirmRegistration from "./pages/ConfirmRegistration";
import MessageSend from "./pages/MessageSend";
const Index = lazy(() => {
  return Promise.all([import("./pages/Index"), new Promise((res) => setTimeout(res, 100))]).then(
    ([moduleExports]) => moduleExports
  );
});

function App() {
  useEffect(() => {
    return () => {
      window.addEventListener("onbeforeunload", () => {
        localStorage.setItem("isAuthTudors", false);
      });
      window.removeEventListener("beforeunload", () => {
        localStorage.setItem("isAuthTudors", false);
      });
    };
  }, []);
  return (
    <AuthState>
      <ProductState>
        <Router>
          <Suspense fallback={<SplashScreen />}>
            <Switch>
              <DefaultLayout path='/' exact component={Index} />
              <DefaultLayout path='/isporuka' component={Isporuka} />
              <DefaultLayout path='/kontakt' component={Kontakt} />
              <DefaultLayout path='/korpa' component={Korpa} />
              <DefaultLayout path='/lokacije' component={Lokacije} />
              <DefaultLayout path='/o-nama' component={ONama} />
              <DefaultLayout path='/proizvod-detalji/:id' component={ProizvodDetalji} />
              <DefaultLayout path='/proizvodi' exact component={Proizvodi} />
              <DefaultLayout path='/proizvodi/:category' component={Proizvodi} />
              <DefaultLayout path='/uslovi-kupovine' component={UsloviKupovine} />
              <DefaultLayout path='/success-order' footerNo component={SuccessOrder} />
              <DefaultLayout path='/success' footerNo component={Success} />
              <DefaultLayout path='/confirm-registration' component={ConfirmRegistration} />
              <DefaultLayout path='/error' footerNo component={Error} />
              <DefaultLayout path='/message-send' footerNo component={MessageSend} />
            </Switch>
          </Suspense>
        </Router>
      </ProductState>
    </AuthState>
  );
}

export default App;
