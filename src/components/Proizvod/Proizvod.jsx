import React from "react";

import { Link } from "react-router-dom";

const Proizvod = ({ link, price, oldPrice, name, id, cover }) => {
  // const image = require(`../../${cover}`);
  // console.log(image);
  return (
    <div className='col-lg-3 col-md-4 col-sm-4'>
      <Link to={`/proizvod-detalji/${id}`}>
        <div className='ponuda-container'>
          <div className='top-line'></div>

          <div
            className='img-container'
            style={{
              backgroundImage: `url(https://admin.tudorsshop.me/${cover})`,
            }}></div>
          {/* <div
            className='img-container'
            style={{
              backgroundImage: `url(${require("../../img/naslovna/intro-banner.png")})`,
            }}></div> */}

          <div className='text-container'>
            <div className='cijena-proizvoda'>
              <span className='cijena'>{price} €</span>
              <span className='separator'>{oldPrice ? " / " : null}</span>
              <span className='popust'>{oldPrice}</span>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default Proizvod;
