import React from "react";

export const ProizvodIsporuka = ({
  name,
  code,
  size,
  price,
  amount,
  remove,
  cover
}) => {
  return (
    <div className="row proizvod-row">
      <div className="col-12">
        <div className="proizvod-container">
          <div className="d-close m-h-3">
            <button onClick={remove} type="button" className="close">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="black"
                width="24px"
                height="24px"
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
              </svg>
            </button>
          </div>

          <div className="row">
            <div className="col-12 d-h-3">
              <div className="m-close w-100 d-flex justify-content-end">
                <button type="button" className="close">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="black"
                    width="24px"
                    height="24px"
                  >
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
                  </svg>
                </button>
              </div>
            </div>

            <div className="col-lg-4 col-md-3">
              <div
                className="proizvod-img"
                style={{
                  backgroundImage: `url(https://admin.tudorsshop.me/${cover}`,
                }}
              ></div>
            </div>

            <div className="col-lg-8 col-md-9 c-col">
              <div className="row row-naslov">
                <div className="col-12">
                  <div className="model-title d-flex flex-column">
                    <h3>{name}</h3>
                  </div>

                  <div className="sifra">
                    Šifra proizvoda: <span>{code}</span>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-12">
                  <div className="line mt-10 mb-15"></div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-12 col-md-6 col-12">
                  <div className="model h-100">
                    <ul>
                      <li>
                        <div className="custom-width">Veličina:</div>
                        <span>{size}</span>
                      </li>
                      <li>
                        <div className="custom-width">Model:</div>
                        <span>Slim Fit</span>
                      </li>
                      <li>
                        <div className="custom-width">Cijena:</div>
                        <div className="price">
                          <span>{price}€</span>
                          <span className="separator"></span>
                          <span className="popust"></span>
                        </div>
                      </li>
                      <li>
                        <div className="custom-width">Količina:</div>
                        <span>{amount}</span>
                      </li>
                      <li>
                        <div className="custom-width ukupno">Ukupno:</div>
                        <div className="t-price">
                          <span>{amount * price}€</span>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
