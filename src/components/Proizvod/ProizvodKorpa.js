import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export const ProizvodKorpa = ({
  name,
  code,
  size,
  image,
  price,
  remove,
  add,
  decrase,
  itemAmount,
}) => {
  const [amount, setAmount] = useState(itemAmount);

  const notify = (name) => toast(name);

  const increaseAmount = () => {
    setAmount((cur) => cur + 1);
    add();
    notify("Povećana količina+" + name);
  };
  const decreaseAmount = () => {
    if (amount === 1) {
      remove();
    } else {
      setAmount((cur) => cur - 1);
      decrase();
      notify("Smanjena količina+" + name);
    }
  };

  return (
    <div className='row proizvod-row'>
      <ToastContainer autoClose={1000} hideProgressBar />

      <div className='col-12'>
        <div className='proizvod-container'>
          <div className='d-close m-h-3'>
            <button onClick={remove} type='button' className='close'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill='black'
                width='24px'
                height='24px'>
                <path d='M0 0h24v24H0V0z' fill='none' />
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z' />
              </svg>
            </button>
          </div>

          <div className='row'>
            <div className='col-12 d-h-3'>
              <div className='m-close w-100 d-flex justify-content-end'>
                <button type='button' className='close'>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 24 24'
                    fill='black'
                    width='24px'
                    height='24px'>
                    <path d='M0 0h24v24H0V0z' fill='none' />
                    <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z' />
                  </svg>
                </button>
              </div>
            </div>

            <div className='col-lg-2 col-md-4'>
              <div className='img-p'>
                <div
                  className='proizvod-img'
                  style={{
                    backgroundImage: `url(https://admin.tudorsshop.me/${image})`,
                  }}></div>
              </div>
            </div>

            <div className='col-lg-10 col-md-8 c-col'>
              <div className='row row-naslov'>
                <div className='col-12'>
                  <div className='model-title d-flex flex-column'>
                    <h3>{name}</h3>
                  </div>

                  <div className='sifra'>
                    Šifra proizvoda: <span>{code}</span>
                  </div>
                </div>
              </div>

              <div className='row'>
                <div className='col-12'>
                  <div className='line my-35'></div>
                </div>
              </div>

              <div className='row'>
                <div className='col-lg-4 col-md-6'>
                  <div className='model h-100'>
                    <ul>
                      <li>
                        <div className='custom-width'>Veličina:</div>
                        <span>{size}</span>
                      </li>
                      <li>
                        <div className='custom-width'>Model:</div>
                        <span>{name}</span>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className='col-lg-4 col-md-6'>
                  <div className='model h-100'>
                    <ul>
                      <li>
                        <div className='custom-width'>Cijena:</div>
                        <div className='price'>
                          <span>{price} €</span>
                        </div>
                      </li>
                      <li>
                        <div className='custom-width'>Količina:</div>
                        <div className='kolicina d-flex align-items-center'>
                          <button
                            onClick={decreaseAmount}
                            className='icon-container d-flex justify-content-center align-items-center'>
                            <svg
                              xmlns='http://www.w3.org/2000/svg'
                              viewBox='0 0 24 24'
                              fill='black'
                              width='18px'
                              height='18px'>
                              <path d='M0 0h24v24H0V0z' fill='none' />
                              <path d='M19 13H5v-2h14v2z' />
                            </svg>
                          </button>
                          <input
                            id='cart_amount_input'
                            className='cart_amount_input'
                            min='1'
                            type='number'
                            value={amount}
                            onChange={(e) => {
                              setAmount(e.target.value);
                            }}
                          />
                          <button
                            onClick={increaseAmount}
                            className='icon-container d-flex justify-content-center align-items-center'>
                            <svg
                              xmlns='http://www.w3.org/2000/svg'
                              viewBox='0 0 24 24'
                              fill='black'
                              width='18px'
                              height='18px'>
                              <path d='M0 0h24v24H0V0z' fill='none' />
                              <path d='M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z' />
                            </svg>
                          </button>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className='col-lg-4 col-md-12'>
                  <div className='cijena-ukupno-container'>
                    <div className='cijena-ukupno'>
                      Cijena ukupno:
                      <span>{(amount * price).toFixed(2)} €</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
