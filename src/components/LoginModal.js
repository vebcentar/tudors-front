import React, { useState, useEffect, useContext } from "react";

import { Link } from "react-router-dom";
import axios from "axios";
import AuthContext from "../context/authContext/authContext";
// import { CloseButton } from "react-toastify/dist/components";

const emailReg = /\S+@\S+\.\S+/;
const LoginModal = ({ show, toggleModal }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [data, setData] = useState();
  const [error, setError] = useState("");

  const { isAuthencated, user, login, loginErrors } = useContext(AuthContext);

  useEffect(() => {
    setEmail("");
    setPassword("");
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    var formData = {
      email: email,
      password: password,
    };
    if (email && password) {
      await login(formData);

      if (localStorage.getItem("isAuthTudors")) {
        document.getElementById("closeButton").click();
      } else {
      }
    } else {
    }
  };

  return (
    <div
      className='modal fade'
      id='modal-prijava'
      tabIndex='-1'
      role='dialog'
      aria-labelledby='modal-label-prijava'
      aria-hidden='true'>
      <div className='modal-dialog' role='document'>
        <div className='modal-content'>
          <button
            type='button'
            id='closeButton'
            className='close'
            data-dismiss='modal'
            aria-label='Close'>
            <span aria-hidden='true'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill='black'
                width='24px'
                height='24px'>
                <path d='M0 0h24v24H0V0z' fill='none' />
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z' />
              </svg>
            </span>
          </button>

          <div className='modal-header'>
            <div className='container'>
              <div className='row'>
                <div className='col-12'>
                  <div className='title-container'>
                    <h5 className='d-flex align-items-center modal-title' id='modal-label-prijava'>
                      Prijava
                    </h5>

                    <p>Ukoliko imate korisnički nalog, prijavite se!</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='line m-h-3'></div>

          <div className='modal-body'>
            <div className='container'>
              <div className='row'>
                <div className='col-12'>
                  <form
                    className='modal-form d-flex flex-column justify-content-center align-items-center'
                    onSubmit={handleSubmit}>
                    <input
                      type='text'
                      name='email'
                      placeholder='Email'
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                    <div className='pass'>
                      <div className='icon-view'>
                        <button
                          type='button'
                          onClick={() => {
                            setShowPassword((prev) => !prev);
                          }}>
                          {showPassword ? (
                            <svg
                              className='visibility visibility-on'
                              xmlns='http://www.w3.org/2000/svg'
                              viewBox='0 0 24 24'
                              fill='#cccccc'
                              width='18px'
                              height='18px'>
                              <path d='M0 0h24v24H0V0z' fill='none' />
                              <path d='M12 6c3.79 0 7.17 2.13 8.82 5.5C19.17 14.87 15.79 17 12 17s-7.17-2.13-8.82-5.5C4.83 8.13 8.21 6 12 6m0-2C7 4 2.73 7.11 1 11.5 2.73 15.89 7 19 12 19s9.27-3.11 11-7.5C21.27 7.11 17 4 12 4zm0 5c1.38 0 2.5 1.12 2.5 2.5S13.38 14 12 14s-2.5-1.12-2.5-2.5S10.62 9 12 9m0-2c-2.48 0-4.5 2.02-4.5 4.5S9.52 16 12 16s4.5-2.02 4.5-4.5S14.48 7 12 7z' />
                            </svg>
                          ) : (
                            <svg
                              className='visibility visibility-off'
                              xmlns='http://www.w3.org/2000/svg'
                              viewBox='0 0 24 24'
                              fill='#cccccc'
                              width='18px'
                              height='18px'>
                              <path
                                d='M0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0z'
                                fill='none'
                              />
                              <path d='M12 6c3.79 0 7.17 2.13 8.82 5.5-.59 1.22-1.42 2.27-2.41 3.12l1.41 1.41c1.39-1.23 2.49-2.77 3.18-4.53C21.27 7.11 17 4 12 4c-1.27 0-2.49.2-3.64.57l1.65 1.65C10.66 6.09 11.32 6 12 6zm-1.07 1.14L13 9.21c.57.25 1.03.71 1.28 1.28l2.07 2.07c.08-.34.14-.7.14-1.07C16.5 9.01 14.48 7 12 7c-.37 0-.72.05-1.07.14zM2.01 3.87l2.68 2.68C3.06 7.83 1.77 9.53 1 11.5 2.73 15.89 7 19 12 19c1.52 0 2.98-.29 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45 2.01 3.87zm7.5 7.5l2.61 2.61c-.04.01-.08.02-.12.02-1.38 0-2.5-1.12-2.5-2.5 0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75c-.23.55-.36 1.15-.36 1.78 0 2.48 2.02 4.5 4.5 4.5.63 0 1.23-.13 1.77-.36l.98.98c-.88.24-1.8.38-2.75.38-3.79 0-7.17-2.13-8.82-5.5.7-1.43 1.72-2.61 2.93-3.53z' />
                            </svg>
                          )}
                        </button>
                      </div>
                      <input
                        type={showPassword ? "text" : "password"}
                        id='login-pass'
                        name='pwd'
                        placeholder='Lozinka'
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                      />
                    </div>

                    <div className='error mb-4' style={{ color: "#71041b" }}>
                      <h4>{loginErrors}</h4>
                    </div>
                    {/* <div className='zaboravili-lozinku d-flex'>
                      <span className='mr-2'>Zaboravili ste lozinku?</span>
                      <Link to=''>Zatražite novu!</Link>
                    </div> */}
                    <div className='modal-form-button'>
                      <button className='tr-3'>Prijavite se</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginModal;
