import React from "react";
import { Link } from "react-router-dom";

const Kravate = ({ data, onClick }) => {
  const { id, children, slug, name } = data;

  return (
    <li className='nav-item dropdown dropdown-hover'>
      <button
        className='nav-link'
        id='navbarDropdown'
        data-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'>
        <div className='d-flex align-items-center justify-content-center'>{name.toUpperCase()}</div>
      </button>
      <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
        <div className='container d-flex justify-content-center'>
          <div
            className='row sub-menu'
            style={{ display: "flex", alignItems: "stretch", overflow: "auto" }}>
            <div className='col-lg-2 m-h align-self-start'>
              <div className='title'>
                <Link
                  to={{
                    pathname: `/proizvodi/${slug}`,
                    state: { id: id, name: name },
                    search: `id=${id}&name=${name}`,
                    // param: { id: id, name: name },
                  }}>
                  <h3>{name}</h3>
                </Link>
              </div>
            </div>

            {/* prva kolona */}
            <div className='col-lg-4 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[0] && (
                    <li
                      style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                      onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kravate/${children[0].slug}`,
                          state: { id: children[0].id, name: children[0].name },
                          search: `id=${children[0].id}&name=${children[0].name}`,
                          // param: { id: id, name: name },
                        }}>
                        <h3>{children[0].name}</h3>
                      </Link>
                    </li>
                  )}
                  <li className='d-none d-lg-flex'>
                    <img src={require("../../img/HederKategorije/Kravate Slim.png")} />
                  </li>
                </ul>
              </div>
            </div>

            {/* druga kolona */}
            <div className='col-lg-4 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[1] && (
                    <li
                      style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                      onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kravate/${children[1].slug}`,
                          state: { id: children[1].id, name: children[1].name },
                          search: `id=${children[1].id}&name=${children[1].name}`,
                          // param: { id: id, name: name },
                        }}>
                        <h3>{children[1].name}</h3>
                      </Link>
                    </li>
                  )}
                  <li className='d-none d-lg-flex'>
                    <img
                      style={{ width: "100%" }}
                      src={require("../../img/HederKategorije/Kravate Classic.png")}
                    />
                  </li>
                </ul>
              </div>
            </div>

            {/* treca kolona */}
            {/* <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[2] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kravate/${children[2].slug}`,
                          state: { id: children[2].id, name: children[2].name },
                          search: `id=${children[2].id}&name=${children[2].name}`,
                        }}>
                        <h3>{children[2].name}</h3>
                      </Link>
                    </li>
                  )}
                  {children[2].children &&
                    children[2].children.map((item) => {
                      return (
                        <li key={item.id} onClick={onClick}>
                          <Link
                            to={{
                              pathname: `/proizvodi/kravate/${item.slug}`,
                              state: {
                                id: item.id,
                                name: item.name,
                              },
                              search: `id=${item.id}&name=${item.name}`,
                            }}>
                            {item.name}
                          </Link>
                        </li>
                      );
                    })}
                </ul>
              </div>
            </div> */}

            {/* cetvrta kolona */}

            {/* slika */}
            {/* <div className='col-lg-2 m-h'>
              <div className='category-img'>
                <img
                  src={
                    id === 580 || id === 584 || id === 589 || id === 614 || id === 632
                      ? require(`../../img/navigacija/${id}.jpg`)
                      : require(`../../img/navigacija/588.jpg`)
                  }
                  alt='drop menu'
                />
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </li>
  );
};

export default Kravate;
