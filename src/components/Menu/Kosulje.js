import React, { useEfe } from "react";
import { Link } from "react-router-dom";

const Kosulje = ({ data, onClick }) => {
  const { id, children, slug, name } = data;

  return (
    <li className='nav-item dropdown dropdown-hover'>
      <button
        className='nav-link'
        id='navbarDropdown'
        data-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'>
        <div className='d-flex align-items-center justify-content-center'>{name.toUpperCase()}</div>
      </button>
      <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
        <div className='container d-flex justify-content-center'>
          <div
            className='row sub-menu'
            style={{ display: "flex", alignItems: "stretch", overflow: "auto" }}>
            <div className='col-lg-2 m-h align-self-start'>
              <div className='title'>
                <Link
                  to={{
                    pathname: `/proizvodi/${slug}`,
                    state: { id: id, name: name },
                    search: `id=${id}&name=${name}`,
                    // param: { id: id, name: name },
                  }}>
                  <h3>{name}</h3>
                </Link>
              </div>
            </div>

            {/* prva kolona */}
            <div className='col-lg-3 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[0] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kosulje/${children[0].slug}`,
                          state: {
                            id: children[0].id,
                            name: children[0].name,
                          },
                          search: `id=${children[0].id}&name=${children[0].name}`,
                        }}>
                        <h3>{children[0].name}</h3>
                      </Link>
                    </li>
                  )}
                  {/* {children[0].children.length &&
                    children[0].children.map((item) => (
                      <li key={item.id} onClick={onClick}>
                        <Link
                          to={{
                            pathname: `/proizvodi/kosulje/${item.slug}`,
                            state: {
                              id: item.id,
                              name: item.name,
                            },
                            search: `id=${item.id}&name=${item.name}`,
                          }}>
                          {item.name}
                        </Link>
                      </li>
                    ))} */}
                  <li className='d-none d-lg-flex'>
                    <img src={require("../../img/HederKategorije/Slim.png")} />
                  </li>
                </ul>
              </div>
            </div>

            {/* druga kolona */}
            <div className='col-lg-3 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[1] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kosulje/${children[1].slug}`,
                          state: {
                            id: children[1].id,
                            name: children[1].name,
                          },
                          search: `id=${children[1].id}&name=${children[1].name}`,
                        }}>
                        <h3>{children[1].name}</h3>
                      </Link>
                    </li>
                  )}
                  {/* {children[1].children.length &&
                    children[1].children.map((item) => (
                      <li key={item.id} onClick={onClick}>
                        <Link
                          to={{
                            pathname: `/proizvodi/kosulje/${item.slug}`,
                            state: {
                              id: item.id,
                              name: item.name,
                            },
                            search: `id=${item.id}&name=${item.name}`,
                          }}>
                          {item.name}
                        </Link>
                      </li>
                    ))} */}
                  <li className='d-none d-lg-flex'>
                    <img src={require("../../img/HederKategorije/Classic.png")} />
                  </li>
                </ul>
              </div>
            </div>

            {/* treca kolona */}
            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[2] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kosulje/${children[2].slug}`,
                          state: {
                            id: children[2].id,
                            name: children[2].name,
                          },
                          search: `id=${children[2].id}&name=${children[2].name}`,
                        }}>
                        <h3>{children[2].name}</h3>
                      </Link>
                    </li>
                  )}
                  <li className='d-none d-lg-flex'>
                    <img src={require("../../img/HederKategorije/Plus size.png")} />
                  </li>
                </ul>
              </div>
            </div>

            {/* cetvrta kolona */}
            {/* <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[3] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/kosulje/${children[3].slug}`,
                          state: {
                            id: children[3].id,
                            name: children[3].name,
                          },
                          search: `id=${children[3].id}&name=${children[3].name}`,
                        }}>
                        <h3>{children[3].name}</h3>
                      </Link>
                    </li>
                  )}
                  {children[3].children.length &&
                    children[3].children.map((item) => (
                      <li key={item.id} onClick={onClick}>
                        <Link
                          to={{
                            pathname: `/proizvodi/kosulje/${item.slug}`,
                            state: {
                              id: item.id,
                              name: item.name,
                            },
                            search: `id=${item.id}&name=${item.name}`,
                          }}>
                          {item.name}
                        </Link>
                      </li>
                    ))}
                </ul>
              </div>
            </div> */}

            {/* slika */}
            {/* <div className='col-lg-2 m-h'>
              <div className='category-img'>
                <img
                  src={
                    id === 580 || id === 584 || id === 589 || id === 614 || id === 632
                      ? require(`../../img/navigacija/${id}.jpg`)
                      : require(`../../img/navigacija/588.jpg`)
                  }
                  alt='drop menu'
                />
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </li>
  );
};

export default Kosulje;
