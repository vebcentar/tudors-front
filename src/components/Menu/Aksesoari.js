import React from "react";
import { Link } from "react-router-dom";

const Aksesoari = ({ data, onClick }) => {
  const { id, children, slug, name } = data;

  return (
    <li className='nav-item dropdown dropdown-hover'>
      <button
        className='nav-link'
        id='navbarDropdown'
        data-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'>
        <div className='d-flex align-items-center justify-content-center'>{name.toUpperCase()}</div>
      </button>
      <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
        <div className='container d-flex justify-content-center'>
          <div
            className='row sub-menu'
            style={{ display: "flex", alignItems: "stretch", overflow: "auto" }}>
            <div className='col-lg-2 m-h align-self-start'>
              <div className='title'>
                <Link
                  to={{
                    pathname: `/proizvodi/${slug}`,
                    state: { id: id, name: name },
                    search: `id=${id}&name=${name}`,
                    // param: { id: id, name: name },
                  }}>
                  <h3>{name}</h3>
                </Link>
              </div>
            </div>

            {/* prva kolona */}
            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[15] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[15].slug}`,
                          state: {
                            id: children[15].id,
                            name: children[15].name,
                          },
                          search: `id=${children[15].id}&name=${children[15].name}`,
                        }}>
                        <h3> {children[15].name}</h3>
                      </Link>
                    </li>
                  )}
                  <li className='d-none d-lg-flex'>
                    <img
                      style={{ width: "100%" }}
                      src={require("../../img/HederKategorije/Kaisevi.png")}
                    />
                  </li>
                </ul>
              </div>
            </div>

            {/* druga kolona */}
            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[0] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[0].slug}`,
                          state: {
                            id: children[0].id,
                            name: children[0].name,
                          },
                          search: `id=${children[0].id}&name=${children[0].name}`,
                        }}>
                        <h3>{children[0].name}</h3>
                      </Link>
                    </li>
                  )}
                  <li className='d-none d-lg-flex'>
                    <img
                      style={{ width: "100%" }}
                      src={require("../../img/HederKategorije/Masne.png")}
                    />
                  </li>
                </ul>
              </div>
            </div>

            {/* treca kolona */}
            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {children[2] && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[2].slug}`,
                          state: {
                            id: children[2].id,
                            name: children[2].name,
                          },
                          search: `id=${children[2].id}&name=${children[2].name}`,
                        }}>
                        <h3>{children[2].name}</h3>
                      </Link>
                    </li>
                  )}
                  <li className='d-none d-lg-flex'>
                    <img
                      style={{ width: "100%" }}
                      src={require("../../img/HederKategorije/Tregeri.png")}
                    />
                  </li>
                </ul>
              </div>
            </div>

            {/* cetvrta kolona */}
            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {name && (
                    <li onClick={onClick}>
                      <Link
                        style={{ color: "#71041b", marginBottom: "30px", fontSize: "12px" }}
                        to={{
                          pathname: `/proizvodi/${slug}`,
                          state: { id: id, name: name },
                          search: `id=${id}&name=${name}`,
                          // param: { id: id, name: name },
                        }}>
                        <h3>Ostali aksesoari</h3>
                      </Link>
                    </li>
                  )}

                  {children[1] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[1].slug}`,
                          state: {
                            id: children[1].id,
                            name: children[1].name,
                          },
                          search: `id=${children[1].id}&name=${children[1].name}`,
                        }}>
                        {children[1].name}
                      </Link>
                    </li>
                  )}
                  {children[3] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[3].slug}`,
                          state: {
                            id: children[3].id,
                            name: children[3].name,
                          },
                          search: `id=${children[3].id}&name=${children[3].name}`,
                        }}>
                        {children[3].name}
                      </Link>
                    </li>
                  )}
                  {children[4] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[4].slug}`,
                          state: {
                            id: children[4].id,
                            name: children[4].name,
                          },
                          search: `id=${children[4].id}&name=${children[4].name}`,
                        }}>
                        {children[4].name}
                      </Link>
                    </li>
                  )}
                  {children[5] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[5].slug}`,
                          state: {
                            id: children[5].id,
                            name: children[5].name,
                          },
                          search: `id=${children[5].id}&name=${children[5].name}`,
                        }}>
                        {children[5].name}
                      </Link>
                    </li>
                  )}
                  {children[6] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[6].slug}`,
                          state: {
                            id: children[6].id,
                            name: children[6].name,
                          },
                          search: `id=${children[6].id}&name=${children[6].name}`,
                        }}>
                        {children[6].name}
                      </Link>
                    </li>
                  )}
                  {children[7] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[7].slug}`,
                          state: {
                            id: children[7].id,
                            name: children[7].name,
                          },
                          search: `id=${children[7].id}&name=${children[7].name}`,
                        }}>
                        {children[7].name}
                      </Link>
                    </li>
                  )}
                  {children[8] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[8].slug}`,
                          state: {
                            id: children[8].id,
                            name: children[8].name,
                          },
                          search: `id=${children[8].id}&name=${children[8].name}`,
                        }}>
                        {children[8].name}
                      </Link>
                    </li>
                  )}
                  {children[9] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[9].slug}`,
                          state: {
                            id: children[9].id,
                            name: children[9].name,
                          },
                          search: `id=${children[9].id}&name=${children[9].name}`,
                        }}>
                        {children[9].name}
                      </Link>
                    </li>
                  )}
                  {children[10] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[10].slug}`,
                          state: {
                            id: children[10].id,
                            name: children[10].name,
                          },
                          search: `id=${children[10].id}&name=${children[10].name}`,
                        }}>
                        {children[10].name}
                      </Link>
                    </li>
                  )}
                  {children[11] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[11].slug}`,
                          state: {
                            id: children[11].id,
                            name: children[11].name,
                          },
                          search: `id=${children[11].id}&name=${children[11].name}`,
                        }}>
                        {children[11].name}
                      </Link>
                    </li>
                  )}
                  {children[12] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[12].slug}`,
                          state: {
                            id: children[12].id,
                            name: children[12].name,
                          },
                          search: `id=${children[12].id}&name=${children[12].name}`,
                        }}>
                        {children[12].name}
                      </Link>
                    </li>
                  )}
                  {children[13] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[13].slug}`,
                          state: {
                            id: children[13].id,
                            name: children[13].name,
                          },
                          search: `id=${children[13].id}&name=${children[13].name}`,
                        }}>
                        {children[13].name}
                      </Link>
                    </li>
                  )}
                  {children[14] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[14].slug}`,
                          state: {
                            id: children[14].id,
                            name: children[14].name,
                          },
                          search: `id=${children[14].id}&name=${children[14].name}`,
                        }}>
                        {children[14].name}
                      </Link>
                    </li>
                  )}
                  {children[15] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[15].slug}`,
                          state: {
                            id: children[15].id,
                            name: children[15].name,
                          },
                          search: `id=${children[15].id}&name=${children[15].name}`,
                        }}>
                        {children[15].name}
                      </Link>
                    </li>
                  )}
                  {children[16] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[16].slug}`,
                          state: {
                            id: children[16].id,
                            name: children[16].name,
                          },
                          search: `id=${children[16].id}&name=${children[16].name}`,
                        }}>
                        {children[16].name}
                      </Link>
                    </li>
                  )}
                  {children[17] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[17].slug}`,
                          state: {
                            id: children[17].id,
                            name: children[17].name,
                          },
                          search: `id=${children[17].id}&name=${children[17].name}`,
                        }}>
                        {children[17].name}
                      </Link>
                    </li>
                  )}
                  {children[18] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[18].slug}`,
                          state: {
                            id: children[18].id,
                            name: children[18].name,
                          },
                          search: `id=${children[18].id}&name=${children[18].name}`,
                        }}>
                        {children[18].name}
                      </Link>
                    </li>
                  )}
                  {children[19] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[19].slug}`,
                          state: {
                            id: children[19].id,
                            name: children[19].name,
                          },
                          search: `id=${children[19].id}&name=${children[19].name}`,
                        }}>
                        {children[19].name}
                      </Link>
                    </li>
                  )}
                  {children[20] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[20].slug}`,
                          state: {
                            id: children[20].id,
                            name: children[20].name,
                          },
                          search: `id=${children[20].id}&name=${children[20].name}`,
                        }}>
                        {children[20].name}
                      </Link>
                    </li>
                  )}
                  {children[21] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[21].slug}`,
                          state: {
                            id: children[21].id,
                            name: children[21].name,
                          },
                          search: `id=${children[21].id}&name=${children[21].name}`,
                        }}>
                        {children[21].name}
                      </Link>
                    </li>
                  )}

                  {children[22] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[22].slug}`,
                          state: {
                            id: children[22].id,
                            name: children[22].name,
                          },
                          search: `id=${children[22].id}&name=${children[22].name}`,
                        }}>
                        {children[22].name}
                      </Link>
                    </li>
                  )}
                  {children[23] && (
                    <li onClick={onClick}>
                      <Link
                        to={{
                          pathname: `/proizvodi/aksesoari/${children[23].slug}`,
                          state: {
                            id: children[23].id,
                            name: children[23].name,
                          },
                          search: `id=${children[23].id}&name=${children[23].name}`,
                        }}>
                        {children[23].name}
                      </Link>
                    </li>
                  )}
                </ul>
              </div>
            </div>

            {/* slika */}
            {/* <div className='col-lg-2 m-h'>
              <div className='category-img'>
                <img
                  src={
                    id === 580 || id === 584 || id === 589 || id === 614 || id === 632
                      ? require(`../../img/navigacija/${id}.jpg`)
                      : require(`../../img/navigacija/588.jpg`)
                  }
                  alt='drop menu'
                />
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </li>
  );
};

export default Aksesoari;
