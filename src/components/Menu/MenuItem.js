import React from "react";
import { Link } from "react-router-dom";

const MenuItem = ({ data }) => {
  const { name, children, slug, id } = data;

  let firstColumn = [];
  let secondColumn = [];
  let thirdColumn = [];
  let fourthColumn = [];

  const numberPerColumns = Math.ceil(children.length / 4);

  if (children) {
    children.forEach((item, idx) => {
      if (children.length < 5) {
        firstColumn.push(item);
      } else {
        if (idx < numberPerColumns) {
          firstColumn.push(item);
        } else if (idx >= numberPerColumns && idx < numberPerColumns * 2) {
          secondColumn.push(item);
        } else if (idx >= numberPerColumns * 2 && idx < numberPerColumns * 3) {
          thirdColumn.push(item);
        } else if (idx >= numberPerColumns * 3) {
          fourthColumn.push(item);
        }
      }
    });
  }

  return (
    <li className='nav-item dropdown dropdown-hover'>
      <button
        className='nav-link'
        id='navbarDropdown'
        data-toggle='dropdown'
        aria-haspopup='true'
        aria-expanded='false'>
        <div className='d-flex align-items-center justify-content-center'>{name.toUpperCase()}</div>
      </button>
      <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
        <div className='container d-flex justify-content-center'>
          <div className='row sub-menu'>
            <div className='col-lg-2 m-h '>
              <div className='title'>
                <Link
                  to={{
                    pathname: `/proizvodi/${slug}`,
                    state: { id: id, name: name },
                    search: `id=${id}&name=${name}`,
                    // param: { id: id, name: name },
                  }}>
                  <h3>{name}</h3>
                </Link>
              </div>
            </div>

            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {firstColumn.map((item) => (
                    <li key={item.id}>
                      <Link
                        to={{
                          pathname: `/proizvodi/${slug}/${item.slug}`,
                          state: {
                            id: item.id,
                            name: item.name,
                          },
                          search: `id=${item.id}&name=${item.name}`,
                        }}>
                        {item.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {secondColumn.map((item) => (
                    <li key={item.id}>
                      <Link
                        to={{
                          pathname: `/proizvodi/${slug}/${item.slug}`,
                          state: {
                            id: item.id,
                            name: item.name,
                          },
                          search: `id=${item.id}&name=${item.name}`,
                        }}>
                        {item.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {thirdColumn.map((item) => (
                    <li key={item.id}>
                      <Link
                        to={{
                          pathname: `/proizvodi/${slug}/${item.slug}`,
                          state: {
                            id: item.id,
                            name: item.name,
                          },
                          search: `id=${item.id}&name=${item.name}`,
                        }}>
                        {item.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className='col-lg-2 col-12 text-left'>
              <div className='links'>
                <ul>
                  {fourthColumn.map((item) => (
                    <li key={item.id}>
                      <Link
                        to={{
                          pathname: `/proizvodi/${slug}/${item.slug}`,
                          state: {
                            id: item.id,
                            name: item.name,
                          },
                          search: `id=${item.id}&name=${item.name}`,
                        }}>
                        {item.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            {/* slika */}
            <div className='col-lg-2 m-h'>
              <div className='category-img'>
                <img
                  src={
                    id === 580 || id === 584 || id === 589 || id === 614 || id === 632
                      ? require(`../../img/navigacija/${id}.jpg`)
                      : require(`../../img/navigacija/588.jpg`)
                  }
                  alt='drop menu'
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
  );
};

export default MenuItem;
