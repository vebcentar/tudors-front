import React from "react";
import { Link } from "react-router-dom";

const Dukserice = ({ data, onClick }) => {
  const { id, children, slug, name } = data;

  return (
    <li className='nav-item' onClick={onClick}>
      <button
        className='nav-link dukserice'
        // id='navbarDropdown'
        // data-toggle='dropdown'
        // aria-haspopup='true'
        // aria-expanded='false'
      >
        <Link
          className='dukserice-link'
          to={{
            pathname: `/proizvodi/${slug}`,
            state: { id: id, name: name },
            search: `id=${id}&name=${name}`,
            // param: { id: id, name: name },
          }}>
          <div className='d-flex align-items-center justify-content-center'>
            {name.toUpperCase()}
          </div>
        </Link>
      </button>
    </li>
  );
  //   <li className='nav-item dropdown dropdown-hover'>
  //     <button
  //       className='nav-link'
  //       id='navbarDropdown'
  //       data-toggle='dropdown'
  //       aria-haspopup='true'
  //       aria-expanded='false'>
  //       <div className='d-flex align-items-center justify-content-center'>{name.toUpperCase()}</div>
  //     </button>
  //     <div className='dropdown-menu' aria-labelledby='navbarDropdown'>
  //       <div className='container d-flex justify-content-center'>
  //         <div className='row sub-menu' style={{ alignItems: "flex-start" }}>
  //           <div className='col-lg-2 m-h '>
  //             <div className='title'>
  //               <Link
  //                 to={{
  //                   pathname: `/proizvodi/${slug}`,
  //                   state: { id: id, name: name },
  //                   search: `id=${id}&name=${name}`,
  //                   // param: { id: id, name: name },
  //                 }}>
  //                 <h3>{name}</h3>
  //               </Link>
  //             </div>
  //           </div>

  //           <div className='col-lg-8 col-12 text-left'>
  //             <div className='links'>
  //               <ul>
  //                 {children[0] && (
  //                   <li>
  //                     <Link
  //                       to={{
  //                         pathname: `/proizvodi/dukserice/${children[0].slug}`,
  //                         state: {
  //                           id: children[0].id,
  //                           name: children[0].name,
  //                         },
  //                         search: `id=${children[0].id}&name=${children[0].name}`,
  //                       }}>
  //                       {children[0].name}
  //                     </Link>
  //                   </li>
  //                 )}
  //                 {children[1] && (
  //                   <li>
  //                     <Link
  //                       to={{
  //                         pathname: `/proizvodi/dukserice/${children[1].slug}`,
  //                         state: {
  //                           id: children[1].id,
  //                           name: children[1].name,
  //                         },
  //                         search: `id=${children[1].id}&name=${children[1].name}`,
  //                       }}>
  //                       {children[1].name}
  //                     </Link>
  //                   </li>
  //                 )}
  //                 {children[2] && (
  //                   <li>
  //                     <Link
  //                       to={{
  //                         pathname: `/proizvodi/dukserice/${children[2].slug}`,
  //                         state: {
  //                           id: children[2].id,
  //                           name: children[2].name,
  //                         },
  //                         search: `id=${children[2].id}&name=${children[2].name}`,
  //                       }}>
  //                       {children[2].name}
  //                     </Link>
  //                   </li>
  //                 )}
  //               </ul>
  //             </div>
  //           </div>

  //           {/* slika */}
  //           <div className='col-lg-2 m-h'>
  //             <div className='category-img'>
  //               <img
  //                 src={
  //                   id === 580 || id === 584 || id === 589 || id === 614 || id === 632
  //                     ? require(`../../img/navigacija/${id}.jpg`)
  //                     : require(`../../img/navigacija/588.jpg`)
  //                 }
  //                 alt='drop menu'
  //               />
  //             </div>
  //           </div>
  //         </div>
  //       </div>
  //     </div>
  //   </li>
  // );
};

export default Dukserice;
