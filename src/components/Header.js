import React, { useState, useEffect, useContext, Children, useRef } from "react";
import { Link, useHistory } from "react-router-dom";
import LoginModal from "./LoginModal";
import MenuItem from "./Menu/MenuItem";
import RegisterModal from "./RegisterModal";
import ProductsContext from "../context/productsContext/productstContext";
import AuthContext from "../context/authContext/authContext";

import axios from "axios";
import Majice from "./Menu/Majice";
import Kosulje from "./Menu/Kosulje";
import Trikotaza from "./Menu/Trikotaza";
import Dukserice from "./Menu/Dukserice";
import Kravate from "./Menu/Kravate";
import Aksesoari from "./Menu/Aksesoari";
import FloatingCart from "./FloatingCart/FloatingCart";

const url = process.env.REACT_APP_TUDORS_API;
const Header = () => {
  const hamburgerRef = useRef(null);
  const history = useHistory();
  const [scrolled, setScrolled] = useState(false);
  const [search, setSearch] = useState("");
  const [floatingCart, setFloatingCart] = useState(false);

  const changeHeaderClasses = () => {
    let topBar = document.querySelector(".top-bar");
    if (window.scrollY > topBar.offsetHeight) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };

  useEffect(() => {
    document.addEventListener("scroll", changeHeaderClasses);
  }, []);

  // const [categories, setCategories] = useState();
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  const [showLoginModal, setShowLoginModal] = useState(false);
  const { cart, searchProducts, searchedProducts, loadCategories, categories } = useContext(
    ProductsContext
  );

  const { isAuthencated, user, logout } = useContext(AuthContext);
  useEffect(() => {
    axios
      .post(`${url}/api/products/getAllProductCategory`)
      .then((res) => {
        console.log(res.data.categories)
        // setCategories(res.data.categories);
        loadCategories(res.data.categories);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    if (floatingCart) {
      document.documentElement.style.overflow = "hidden";
      return () => (document.documentElement.style.overflow = "unset");
    }
  }, [floatingCart]);

  const submit = (e) => {
    e.preventDefault();
    localStorage.setItem("searchValue", search);
    /* window.href = "/proizvodi"; */
    searchProducts(search);
    // console.log(history);
    history.push("/proizvodi");
  };

  const clickDiv = () => {
    hamburgerRef.current.click();
  };

  const findIndexOfCategory = (id) => {
    const catIndex = categories.findIndex((el) => el.id === id);
    return categories[catIndex];
  };

  return (
    <header>
      <LoginModal show={showLoginModal} toggleModal={() => setShowLoginModal((c) => !c)} />
      <RegisterModal show={showRegisterModal} toggleModal={() => setShowRegisterModal((c) => !c)} />
      <div className='container-fluid menu'>
        <div className={`top-bar ${scrolled ? "scrolled" : ""}`}>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-5'>
                <div className='registracija-prijava'>
                  {isAuthencated ? (
                    <div>
                      <h2 className=''>{user.name}</h2>
                      <button onClick={logout}>
                        <h4>Odjavi se</h4>
                      </button>
                    </div>
                  ) : (
                    <>
                      <button
                        data-toggle='modal'
                        data-target='#modal-registracija'
                        onClick={() => {
                          setShowRegisterModal(true);
                        }}>
                        <div className='item-container d-flex flex-row align-items-center'>
                          <svg
                            xmlns='http://www.w3.org/2000/svg'
                            viewBox='0 0 24 24'
                            fill='black'
                            width='18px'
                            height='18px'>
                            <path d='M0 0h24v24H0V0z' fill='none' />
                            <path d='M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM7.07 18.28c.43-.9 3.05-1.78 4.93-1.78s4.51.88 4.93 1.78C15.57 19.36 13.86 20 12 20s-3.57-.64-4.93-1.72zm11.29-1.45c-1.43-1.74-4.9-2.33-6.36-2.33s-4.93.59-6.36 2.33C4.62 15.49 4 13.82 4 12c0-4.41 3.59-8 8-8s8 3.59 8 8c0 1.82-.62 3.49-1.64 4.83zM12 6c-1.94 0-3.5 1.56-3.5 3.5S10.06 13 12 13s3.5-1.56 3.5-3.5S13.94 6 12 6zm0 5c-.83 0-1.5-.67-1.5-1.5S11.17 8 12 8s1.5.67 1.5 1.5S12.83 11 12 11z' />
                          </svg>
                          <div className='text-container'>
                            <span>Registrujte se</span>
                          </div>
                        </div>
                      </button>

                      <button
                        data-toggle='modal'
                        data-target='#modal-prijava'
                        onClick={() => setShowLoginModal(true)}>
                        <div className='item-container d-flex flex-row align-items-center'>
                          <svg
                            xmlns='http://www.w3.org/2000/svg'
                            enableBackground='new 0 0 24 24'
                            viewBox='0 0 24 24'
                            fill='black'
                            width='18px'
                            height='18px'>
                            <g>
                              <rect fill='none' height='24' width='24' />
                            </g>
                            <g>
                              <path d='M11,7L9.6,8.4l2.6,2.6H2v2h10.2l-2.6,2.6L11,17l5-5L11,7z M20,19h-8v2h8c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2h-8v2h8V19z' />
                            </g>
                          </svg>
                          <div className='text-container'>
                            <span>Prijavite se</span>
                          </div>
                        </div>
                      </button>
                    </>
                  )}
                </div>
              </div>
              <div className='col-lg-2 px-0'>
                <div className='logo-container'>
                  <Link className='logo' to='/'>
                    <img
                      src={require("../img/navigacija/tudors-logo.png")}
                      className='logo-img'
                      alt=''
                    />
                  </Link>
                </div>
              </div>
              <div className='col-lg-5'>
                <div className='icons'>
                  {/* <Link to='/korpa' className='m-h korpa-link ml-30'> */}
                  <div className='cart-container mx-10' onClick={() => setFloatingCart(true)}>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 24 24'
                      fill='black'
                      width='20px'
                      height='20px'>
                      <path d='M0 0h24v24H0V0z' fill='none' />
                      <path d='M19 6h-2c0-2.76-2.24-5-5-5S7 3.24 7 6H5c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm-7-3c1.66 0 3 1.34 3 3H9c0-1.66 1.34-3 3-3zm7 17H5V8h14v12zm-7-8c-1.66 0-3-1.34-3-3H7c0 2.76 2.24 5 5 5s5-2.24 5-5h-2c0 1.66-1.34 3-3 3z' />
                    </svg>

                    <div className='badge-container'>
                      <span>{cart.length}</span>
                    </div>
                  </div>
                  {/* </Link> */}
                  {floatingCart && (
                    <FloatingCart show={floatingCart} close={() => setFloatingCart(false)} />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        {/*  */}
        <nav
          className={`navbar navbar-expand-lg navbar-mobile-bg fixed-top ${
            scrolled ? "scrolled top-change" : ""
          }`}>
          <div className='container-fluid navbar-collapse-bg custom-navbar'>
            <div className='mobile-bar d-flex align-items-center'>
              <a className='navbar-brand d-h' href='./'>
                <img
                  src={require("../img/navigacija/tudors-logo.png")}
                  className='logo-img'
                  alt=''
                />
              </a>

              <div className='mobile-bar-icons'>
                <div className='d-flex flex-row'>
                  <div className='toggle-language d-h'>
                    <button className='selected language-container'>ME</button>
                    <button className='option language-container'>EN</button>
                  </div>

                  <Link to='/korpa' className='d-h korpa-link ml-20 mr-30'>
                    <div className='cart-container'>
                      <svg
                        width='14'
                        height='14'
                        viewBox='0 0 1792 1792'
                        xmlns='http://www.w3.org/2000/svg'>
                        <path d='M704 1536q0 52-38 90t-90 38-90-38-38-90 38-90 90-38 90 38 38 90zm896 0q0 52-38 90t-90 38-90-38-38-90 38-90 90-38 90 38 38 90zm128-1088v512q0 24-16.5 42.5t-40.5 21.5l-1044 122q13 60 13 70 0 16-24 64h920q26 0 45 19t19 45-19 45-45 19h-1024q-26 0-45-19t-19-45q0-11 8-31.5t16-36 21.5-40 15.5-29.5l-177-823h-204q-26 0-45-19t-19-45 19-45 45-19h256q16 0 28.5 6.5t19.5 15.5 13 24.5 8 26 5.5 29.5 4.5 26h1201q26 0 45 19t19 45z' />
                      </svg>

                      <div className='badge-container'>
                        <span>{cart.length}</span>
                      </div>
                    </div>
                  </Link>
                  <button
                    ref={hamburgerRef}
                    className='navbar-toggler'
                    type='button'
                    data-toggle='collapse'
                    data-target='#navbarSupportedContent'
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'>
                    <div className='hamburger hamburger--spin'>
                      <div className='hamburger-box'>
                        <div className='hamburger-inner'></div>
                      </div>
                    </div>
                  </button>
                </div>
              </div>
            </div>

            <div className='collapse navbar-collapse' id='navbarSupportedContent'>
              <div className='d-flex flex-column w-100 navbar-nav-container'>
                <ul className='navbar-nav ml-auto mr-auto'>
                  {categories.length && (
                    <>
                      <Majice onClick={clickDiv} data={findIndexOfCategory(580)} />
                      <Kosulje onClick={clickDiv} data={findIndexOfCategory(632)} />
                      <Aksesoari onClick={clickDiv} data={findIndexOfCategory(588)} />
                      <Kravate onClick={clickDiv} data={findIndexOfCategory(589)} />
                      <Dukserice onClick={clickDiv} data={findIndexOfCategory(614)} />
                      <Trikotaza onClick={clickDiv} data={findIndexOfCategory(584)} />
                    </>
                  )}

                  <li className='nav-item nav-item-acc d-h'>
                    <button data-toggle='modal' data-target='#modal-registracija'>
                      <div className='item-container d-flex flex-row align-items-center'>
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 24 24'
                          fill='white'
                          width='18px'
                          height='18px'>
                          <path d='M0 0h24v24H0z' fill='none' />
                          <path d='M3 5v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.11 0-2 .9-2 2zm12 4c0 1.66-1.34 3-3 3s-3-1.34-3-3 1.34-3 3-3 3 1.34 3 3zm-9 8c0-2 4-3.1 6-3.1s6 1.1 6 3.1v1H6v-1z' />
                        </svg>
                        <div className='text-container'>
                          <span>Registracija</span>
                        </div>
                      </div>
                    </button>
                  </li>

                  <li className='nav-item nav-item-acc d-h'>
                    <button data-toggle='modal' data-target='#modal-prijava'>
                      <div className='item-container d-flex flex-row align-items-center'>
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          enableBackground='new 0 0 24 24'
                          viewBox='0 0 24 24'
                          fill='white'
                          width='18px'
                          height='18px'>
                          <g>
                            <rect fill='none' height='24' width='24' />
                          </g>
                          <g>
                            <path d='M11,7L9.6,8.4l2.6,2.6H2v2h10.2l-2.6,2.6L11,17l5-5L11,7z M20,19h-8v2h8c1.1,0,2-0.9,2-2V5c0-1.1-0.9-2-2-2h-8v2h8V19z' />
                          </g>
                        </svg>
                        <div className='text-container'>
                          <span>Prijava</span>
                        </div>
                      </div>
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
