import React from "react";
import { Route } from "react-router-dom";
import Footer from "./Footer";
import Header from "./Header";
const DefaultLayout = ({ footerNo, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(matchProps) => (
        <>
          <Header />
          <Component {...matchProps} />
          <div style={{ display: footerNo ? "none" : null }}>
            <Footer />
          </div>
        </>
      )}
    />
  );
};

export default DefaultLayout;
