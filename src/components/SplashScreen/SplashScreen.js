import React from "react";
import logo from "../../img/navigacija/tudors-logo.png";

const SplashScreen = () => {
  return (
    <div className='splash-screen'>
      <img src={logo} alt='Splash screen logo' />
    </div>
  );
};

export default SplashScreen;
