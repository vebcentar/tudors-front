import React from "react";
import css from "./comingsoon.module.css";
import logo from "../../img/navigacija/tudors-logo.png";

const ComingSoon = () => {
  return (
    <div className={css.bg}>
      <div className={css.top}>
        <img src={logo} alt='' />
      </div>
      <div className={css.bottom}>
        <h1>Coming Soon</h1>
      </div>
    </div>
  );
};

export default ComingSoon;
