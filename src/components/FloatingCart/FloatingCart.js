import React, { useContext } from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";
import { Link, useHistory } from "react-router-dom";
import ProductsContext from "../../context/productsContext/productstContext";
import { FloatingProizvod } from "./FloatingProizvod";

const customStyles = {
  content: {
    position: "absolute",
    zIndex: "1999",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

Modal.setAppElement("#cart-modal");

const FloatingCart = ({ show, close }) => {
  const { cart, removeFromCart, addToCart, decraseAmount } = useContext(
    ProductsContext
  );
  const history = useHistory();
  const totalPrice = cart.reduce(function (earnings, item) {
    return (
      earnings +
      +(item.discount_value
        ? ((1 - item.discount_value / 100) * item.price).toFixed(2)
        : item.price) *
        item.amount
    );
  }, 0);

  const clickHandler = (href) => {
    close();
    history.push(href);
  };

  return (
    <Modal
      isOpen={show}
      onRequestClose={close}
      shouldCloseOnOverlayClick={true}
      shouldCloseOnEsc={true}
      style={{
        overlay: {
          backgroundColor: "rgba(0, 0, 0, 0.5)",
          zIndex: 1060,
          position: "fixed",
          top: 0,
          height: "100vh",
        },
        content: {
          width: "550px",
          right: 0,
          height: "auto",
          maxHeight: "650px",
          inset: "unset",
          padding: "20px 0px",
        },
      }}
    >
      <div className="container">
        <div className="row">
          <div className="col-12 cart-modal-header d-flex align-items-center justify-content-between">
            <h1>Korpa </h1>
            <button type="button" className="close" onClick={close}>
              <span aria-hidden="true">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="black"
                  width="24px"
                  height="24px"
                >
                  <path d="M0 0h24v24H0V0z" fill="none" />
                  <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
                </svg>
              </span>
            </button>
          </div>
          <div className="col-12 mt-3 d-flex align-items-center">
            <h4>Broj proizvoda</h4>
            <div className="cart-modal-count">{cart.length}</div>
          </div>
        </div>
        <div className="cart-modal-divider"></div>
        <div className="cart-modal-proizvodi">
          {cart.map((item) => {
            return (
              <FloatingProizvod
                name={item.name}
                code={item.product_code}
                cover={item.image}
                price={
                  item.discount_value
                    ? ((1 - item.discount_value / 100) * item.price).toFixed(2)
                    : item.price
                }
                itemAmount={item.amount}
                size={item.size}
                color={item.color}
                remove={() => removeFromCart(item)}
                add={() => addToCart(item)}
                decrase={() => decraseAmount(item)}
                key={item.id}
              />
            );
          })}
        </div>
        <div className="col-12 mt-5">
          <h2 className="font-weight-bold cart-modal-ukupno">
            Ukupno: <span>{totalPrice.toFixed(2)} €</span>
          </h2>
        </div>
        <div className="col-12 mt-5 f-s-14">
          Cijena dostave pouzećem:
          <span className="cart-modal-dostava font-weight-bold"> 3.00€</span>
        </div>
        <div className="col-12 f-s-14">
          Isporuku vršimo samo na teritoriji Crne Gore.
        </div>
        <div className="col-12 cart-modal-potvrdite mt-5">
          <button onClick={() => clickHandler("/isporuka")}>NASTAVITE</button>
        </div>
        <div className="cart-modal-divider"></div>
        <div className="col-12 cart-modal-pregled mt-5">
          <button onClick={() => clickHandler("/korpa")}>PREGLED KORPE</button>
        </div>
      </div>
    </Modal>
  );
};

export default FloatingCart;
