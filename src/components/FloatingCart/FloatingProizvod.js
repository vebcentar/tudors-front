import React, { useState } from "react";

export const FloatingProizvod = ({
  name,
  code,
  size,
  price,
  itemAmount,
  remove,
  cover,
  color,
  add,
  decrase,
}) => {
  const [amount, setAmount] = useState(itemAmount);
  const increaseAmount = () => {
    setAmount((cur) => cur + 1);
    add();
  };
  const decreaseAmount = () => {
    if (amount === 1) {
      remove();
    } else {
      setAmount((cur) => cur - 1);
      decrase();
    }
  };
  return (
    <div className='row proizvod-row cart-modal-proizvod'>
      <div className='col-12'>
        <div className='proizvod-container'>
          <div className='d-close m-h-3'>
            <button onClick={remove} type='button' className='close'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill='black'
                width='24px'
                height='24px'>
                <path d='M0 0h24v24H0V0z' fill='none' />
                <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z' />
              </svg>
            </button>
          </div>

          <div className='row'>
            <div className='col-12 d-h-3'>
              <div className='m-close w-100 d-flex justify-content-end'>
                <button type='button' className='close'>
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 24 24'
                    fill='black'
                    width='24px'
                    height='24px'>
                    <path d='M0 0h24v24H0V0z' fill='none' />
                    <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z' />
                  </svg>
                </button>
              </div>
            </div>

            <div className='col-lg-4 p-0'>
              <div
                className='cart-modal-img'
                style={{
                  backgroundImage: `url(https://admin.tudorsshop.me/${cover}`,
                }}></div>
            </div>

            <div className='col-lg-8 c-col'>
              <div className='row row-naslov'>
                <div className='col-12'>
                  <div className='model-title d-flex flex-column'>
                    <h4>{name}</h4>
                  </div>
                </div>
              </div>

              {/* <div className='row'>
                <div className='col-12'>
                  <div className='line mt-10 mb-15'></div>
                </div>
              </div> */}

              <div className='row'>
                <div className='col-12'>
                  <div className='model h-100'>
                    <ul>
                      <li>
                        <div className='text-color-black font-weight-bold f-s-14'>
                          Veličina: <span>{size}</span>
                        </div>
                      </li>
                      <li>
                        <div className='text-color-black font-weight-bold f-s-14'>
                          Boja: <span className='font-weight-normal'>{color}</span>
                        </div>
                      </li>
                      <li>
                        <div className='text-color-black font-weight-bold f-s-14'>
                          Model: <span className='font-weight-normal'>Slim Fit</span>
                        </div>
                      </li>

                      <li>
                        <div className='cart-modal-kolicina d-flex align-items-center justify-content-between'>
                          <div className='cart-modal-kolicina d-flex align-items-center'>
                            <div className='font-weight-bold f-s-14 pt-2'>Količina:</div>
                            <div className='d-flex align-items-center ml-2'>
                              <button
                                onClick={decreaseAmount}
                                className='icon-container d-flex justify-content-center align-items-center'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  viewBox='0 0 18 18'
                                  fill='black'
                                  width='12px'
                                  height='12px'>
                                  <path d='M0 0h24v24H0V0z' fill='none' />
                                  <path d='M19 13H5v-2h14v2z' />
                                </svg>
                              </button>
                              <input
                                id='cart_amount_input'
                                className='cart-modal-proizvod-kolicina'
                                min='1'
                                type='number'
                                value={amount}
                                onChange={(e) => {
                                  setAmount(e.target.value);
                                }}
                              />
                              <button
                                onClick={increaseAmount}
                                className='icon-container d-flex justify-content-center align-items-center'>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  viewBox='0 0 18 18'
                                  fill='black'
                                  width='12px'
                                  height='12px'>
                                  <path d='M0 0h24v24H0V0z' fill='none' />
                                  <path d='M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z' />
                                </svg>
                              </button>
                            </div>
                          </div>
                          <h3 className='cart-modal-proizvod-price'>
                            {(amount * price).toFixed(2)} €
                          </h3>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
