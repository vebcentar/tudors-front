import React, { useEffect, useState, useContext } from "react";
import AuthContext from "../context/authContext/authContext";
import { Link, useHistory } from "react-router-dom";

const numberRegex = new RegExp("^\\d+$");
const RegisterModal = () => {
  const history = useHistory();
  const [showPassword, setShowPassword] = useState(false);
  const [showRepeat, setShowRepeat] = useState(false);
  const [accepted, setAccepted] = useState(false);
  const [acceptedError, setAcceptedError] = useState(false);
  const [showCompany, setShowCompany] = useState(false); //prikaz dodatnih polja za pravna lica
  const [selectedLice, setSelectedLice] = useState(0);
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [password, setPassword] = useState("");
  const [password_confirmation, setPasswordConfirmation] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [email, setEmail] = useState("");
  const [firma, setFirma] = useState("");
  const [pib, setPib] = useState("");
  const [pdv, setPdv] = useState("");

  const { register } = useContext(AuthContext);

  const handleChangeLice = (e) => {
    setSelectedLice(e.target.value);
    if (e.target.value == 1) {
      setShowCompany(true);
    } else {
      setShowCompany(false);
    }
  };
  const resetFields = () => {
    setName("");
    setPhone("");
    setAddress("");
    setCity("");
    setPassword("");
    setPasswordConfirmation("");
    setEmail("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    var formData = {
      email: email,
      password: password,
      name: name,
      password_confirmation: password_confirmation,
      phone: phone,
      address: address,
      city: city,
      type: "0",
    };
    if (numberRegex.test(phone)) {
      setPhoneError("");
      if (password === password_confirmation) {
        setPasswordError("");
/*         if (accepted) {
 */          setAcceptedError(false);
          await register(formData);

          document.getElementById("regCloseButton").click();
          history.push("/confirm-registration");
       /*  } else {
          setAcceptedError(true);
        } */
      } else {
        setPasswordError("Ponovite lozinku");
      }
    } else {
      setPhoneError("Unesite ispravan broj");
    }
  };

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <div
      className="modal fade"
      id="modal-registracija"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal-label-registracija"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <button
            type="button"
            id="regCloseButton"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="black"
                width="24px"
                height="24px"
              >
                <path d="M0 0h24v24H0V0z" fill="none" />
                <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z" />
              </svg>
            </span>
          </button>

          <div className="modal-header">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <div className="title-container">
                    <h5
                      className="d-flex align-items-center modal-title"
                      id="modal-label-registracija"
                    >
                      Registracija
                    </h5>

                    <p>
                      Ukoliko ste novi korisnik, kreirajte Vaš korisnički nalog
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="line m-h-3"></div>

          <div className="modal-body">
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <form
                    className="modal-form d-flex flex-column justify-content-center align-items-center"
                    onSubmit={handleSubmit}
                  >
                    <div className="radio-lica d-flex align-items-center w-100 mb-30">
                      <div className="radio-fizicka-lica d-flex align-items-center">
                        <input
                          type="radio"
                          name="popup-lica"
                          id="popup-fizicka-lica"
                          value={0}
                          checked={selectedLice == 0}
                          onChange={handleChangeLice}
                        />
                        <label htmlFor="popup-fizicka-lica" className="ml-10">
                          Fizičko lice
                        </label>
                      </div>
                      <div className="radio-pravna-lica d-flex align-items-center ml-30">
                        <input
                          type="radio"
                          name="popup-lica"
                          id="popup-pravna-lica"
                          value={1}
                          checked={selectedLice == 1}
                          onChange={handleChangeLice}
                        />
                        <label htmlFor="popup-pravna-lica" className="ml-10">
                          Pravno lice
                        </label>
                      </div>
                    </div>

                    <input
                      type="text"
                      name="grad-mjesto"
                      placeholder="Ime i prezime"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      required
                    />

                    {/* <!-- Pravno lice --> */}
                    {showCompany && (
                      <>
                        <input
                          className="popup-pravno-lice"
                          type="text"
                          name="naziv-firme"
                          placeholder="Naziv firme"
                          value={firma}
                          onChange={(e) => {
                            setFirma(e.target.value);
                          }}
                          required
                        />
                        <div className="d-flex flex-row justify-content-between w-100">
                          <input
                            className="input-half popup-pravno-lice"
                            type="text"
                            name="pib"
                            placeholder="PIB"
                            value={pib}
                            onChange={(e) => {
                              setPib(e.target.value);
                            }}
                            required
                          />
                          <input
                            className="input-half popup-pravno-lice"
                            type="text"
                            name="pdv"
                            placeholder="PDV"
                            value={pdv}
                            onChange={(e) => {
                              setPdv(e.target.value);
                            }}
                            required
                          />
                        </div>
                      </>
                    )}

                    {/* <!-- End --> */}

                    <input
                      type="email"
                      name="email"
                      placeholder="Email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                    <input
                      type="text"
                      name="tel"
                      placeholder="Telefon"
                      value={phone}
                      onChange={(e) => setPhone(e.target.value)}
                      required
                    />
                    {phoneError && (
                      <h5 className="text-danger align-self-start">
                        {phoneError}
                      </h5>
                    )}
                    <input
                      type="text"
                      name="adresa"
                      placeholder="Adresa"
                      value={address}
                      onChange={(e) => setAddress(e.target.value)}
                      required
                    />
                    <input
                      type="text"
                      name="grad"
                      placeholder="Grad"
                      value={city}
                      onChange={(e) => setCity(e.target.value)}
                      required
                    />
                    <div className="pass">
                      <div className="icon-view">
                        <button
                          type="button"
                          onClick={() => {
                            setShowPassword((p) => !p);
                          }}
                        >
                          <svg
                            className="visibility visibility-on d-none"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="#cccccc"
                            width="18px"
                            height="18px"
                          >
                            <path d="M0 0h24v24H0V0z" fill="none" />
                            <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5C19.17 14.87 15.79 17 12 17s-7.17-2.13-8.82-5.5C4.83 8.13 8.21 6 12 6m0-2C7 4 2.73 7.11 1 11.5 2.73 15.89 7 19 12 19s9.27-3.11 11-7.5C21.27 7.11 17 4 12 4zm0 5c1.38 0 2.5 1.12 2.5 2.5S13.38 14 12 14s-2.5-1.12-2.5-2.5S10.62 9 12 9m0-2c-2.48 0-4.5 2.02-4.5 4.5S9.52 16 12 16s4.5-2.02 4.5-4.5S14.48 7 12 7z" />
                          </svg>
                          <svg
                            className="visibility visibility-off"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="#cccccc"
                            width="18px"
                            height="18px"
                          >
                            <path
                              d="M0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0z"
                              fill="none"
                            />
                            <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5-.59 1.22-1.42 2.27-2.41 3.12l1.41 1.41c1.39-1.23 2.49-2.77 3.18-4.53C21.27 7.11 17 4 12 4c-1.27 0-2.49.2-3.64.57l1.65 1.65C10.66 6.09 11.32 6 12 6zm-1.07 1.14L13 9.21c.57.25 1.03.71 1.28 1.28l2.07 2.07c.08-.34.14-.7.14-1.07C16.5 9.01 14.48 7 12 7c-.37 0-.72.05-1.07.14zM2.01 3.87l2.68 2.68C3.06 7.83 1.77 9.53 1 11.5 2.73 15.89 7 19 12 19c1.52 0 2.98-.29 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45 2.01 3.87zm7.5 7.5l2.61 2.61c-.04.01-.08.02-.12.02-1.38 0-2.5-1.12-2.5-2.5 0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75c-.23.55-.36 1.15-.36 1.78 0 2.48 2.02 4.5 4.5 4.5.63 0 1.23-.13 1.77-.36l.98.98c-.88.24-1.8.38-2.75.38-3.79 0-7.17-2.13-8.82-5.5.7-1.43 1.72-2.61 2.93-3.53z" />
                          </svg>
                        </button>
                      </div>
                      <input
                        type={showPassword ? "text" : "password"}
                        id="reg-pass"
                        name="lozinka"
                        placeholder="Lozinka"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                      />
                    </div>

                    <div className="pass-confirm">
                      <div className="icon-view">
                        <button
                          type="button"
                          onClick={() => {
                            setShowRepeat((p) => !p);
                          }}
                        >
                          <svg
                            className="visibility visibility-on d-none"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="#cccccc"
                            width="18px"
                            height="18px"
                          >
                            <path d="M0 0h24v24H0V0z" fill="none" />
                            <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5C19.17 14.87 15.79 17 12 17s-7.17-2.13-8.82-5.5C4.83 8.13 8.21 6 12 6m0-2C7 4 2.73 7.11 1 11.5 2.73 15.89 7 19 12 19s9.27-3.11 11-7.5C21.27 7.11 17 4 12 4zm0 5c1.38 0 2.5 1.12 2.5 2.5S13.38 14 12 14s-2.5-1.12-2.5-2.5S10.62 9 12 9m0-2c-2.48 0-4.5 2.02-4.5 4.5S9.52 16 12 16s4.5-2.02 4.5-4.5S14.48 7 12 7z" />
                          </svg>
                          <svg
                            className="visibility visibility-off"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="#cccccc"
                            width="18px"
                            height="18px"
                          >
                            <path
                              d="M0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0zm0 0h24v24H0V0z"
                              fill="none"
                            />
                            <path d="M12 6c3.79 0 7.17 2.13 8.82 5.5-.59 1.22-1.42 2.27-2.41 3.12l1.41 1.41c1.39-1.23 2.49-2.77 3.18-4.53C21.27 7.11 17 4 12 4c-1.27 0-2.49.2-3.64.57l1.65 1.65C10.66 6.09 11.32 6 12 6zm-1.07 1.14L13 9.21c.57.25 1.03.71 1.28 1.28l2.07 2.07c.08-.34.14-.7.14-1.07C16.5 9.01 14.48 7 12 7c-.37 0-.72.05-1.07.14zM2.01 3.87l2.68 2.68C3.06 7.83 1.77 9.53 1 11.5 2.73 15.89 7 19 12 19c1.52 0 2.98-.29 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45 2.01 3.87zm7.5 7.5l2.61 2.61c-.04.01-.08.02-.12.02-1.38 0-2.5-1.12-2.5-2.5 0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75c-.23.55-.36 1.15-.36 1.78 0 2.48 2.02 4.5 4.5 4.5.63 0 1.23-.13 1.77-.36l.98.98c-.88.24-1.8.38-2.75.38-3.79 0-7.17-2.13-8.82-5.5.7-1.43 1.72-2.61 2.93-3.53z" />
                          </svg>
                        </button>
                      </div>
                      <input
                        type={showRepeat ? "text" : "password"}
                        id="reg-pass-confirm"
                        name="potvrdite-lozinku"
                        placeholder="Potvrdite lozinku"
                        value={password_confirmation}
                        onChange={(e) =>
                          setPasswordConfirmation(e.target.value)
                        }
                        required
                      />
                      {passwordError && (
                        <h4 className="text-danger">{passwordError}</h4>
                      )}
                    </div>

                    {/* <div className="uslovi-koriscenja">
                      <input
                        type="checkbox"
                        name="uslovi"
                        id="uslovi"
                        checked={accepted}
                        onChange={() => setAccepted((c) => !c)}
                      />
                      <label htmlFor="uslovi">
                        Slažem se sa uslovima korišćenja
                      </label>
                    </div>
                    {acceptedError && (
                      <h4 className="text-danger align-self-start">
                        Morate prihvatiti uslove koriscenja
                      </h4>
                    )} */}

                    <div className="modal-form-button">
                      <button className="tr-3">Registrujte se</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterModal;
