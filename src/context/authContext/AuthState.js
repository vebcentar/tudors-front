import React, { useReducer, createContext } from "react";
import authReducer from "../authContext/authReducer";
import AuthContext from "../authContext/authContext";
import axios from "axios";

import { REGISTER_SUCCESS, LOGIN_SUCCESS, LOGIN_FAIL, REGISTER_FAIL, LOGOUT } from "../types";

const AuthState = (props) => {
  const intialState = {
    isAuthencated: localStorage.getItem("isAuthTudors")
      ? localStorage.getItem("isAuthTudors")
      : false,
    user: localStorage.getItem("tudorsUser") ? JSON.parse(localStorage.getItem("tudorsUser")) : {},
    error: null,
    url: `${process.env.REACT_APP_TUDORS_API}api`,
    loginErrors: "",
  };
  const [state, dispatch] = useReducer(authReducer, intialState);

  //Register User

  const register = async (formData) => {
    const config = {
      header: {
        "Content-Type": "application/json",
      },
    };
    try {
      const res = await axios.post(state.url + "/customers/register", formData, config);
      console.log(res);
      if (res.data.success == true) {
        dispatch({
          type: REGISTER_SUCCESS,
          payload: res.data,
        });
      } else {
        dispatch({
          type: REGISTER_FAIL,
          payload: res.data,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };
  //login user
  const login = async (formData) => {
    const config = {
      header: {
        "Content-Type": "application/json",
      },
    };

    try {
      const res = await axios.post(state.url + "/customers/login", formData, config);
      if (res.data.success == true) {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: res.data,
        });
      } else {
        dispatch({
          type: LOGIN_FAIL,
          payload: res.data,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  const logout = () => {
    dispatch({
      type: LOGOUT,
    });
  };
  return (
    <AuthContext.Provider
      value={{
        isAuthencated: state.isAuthencated,
        user: state.user,
        loginErrors: state.loginErrors,
        register,
        login,
        logout,
      }}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
