import { REGISTER_SUCCESS, LOGIN_SUCCESS, REGISTER_FAIL, LOGIN_FAIL, LOGOUT } from "../types";

export default (state, { type, payload }) => {
  switch (type) {
    case REGISTER_SUCCESS:
      localStorage.setItem("tokenTudors", payload.token);
      return {
        ...state,
        ...payload,
        loading: false,
      };
    case LOGIN_SUCCESS:
      localStorage.setItem("tokenTudors", payload.token);
      localStorage.setItem("isAuthTudors", true);
      localStorage.setItem("tudorsUser", JSON.stringify(payload.user));
      return {
        ...state,
        ...payload,
        isAuthencated: true,
        loading: false,
        user: payload.user,
      };
    case LOGOUT:
      localStorage.setItem("isAuthTudors", false);
      localStorage.setItem("tudorsUser", JSON.stringify({}));
      return {
        ...state,
        isAuthencated: false,
        user: {},
      };
    case REGISTER_FAIL:
    case LOGIN_FAIL:
      localStorage.removeItem("tokenTudors");
      localStorage.removeItem("isAuthTudors");
      return {
        ...state,
        token: null,
        isAuthencated: false,
        user: null,
        error: payload,
        loginErrors: payload.message,
      };

    default:
      return state;
  }
};
