import {
  LOAD_PRODUCTS,
  REMOVE_PRODUCTS_FROM_CART,
  ADD_PRODUCTS_TO_CART,
  INCRASE_AMOUNT_IN_CART,
  DECRASE_AMOUNT_IN_CART,
  LOAD_SPECIAL_PRODUCTS,
  LOAD_CATEGORIES,
} from "../types";

export default (state, { type, payload }) => {
  switch (type) {
    case LOAD_PRODUCTS:
      return {
        ...state,
        products: payload,
      };
    case LOAD_SPECIAL_PRODUCTS:
      return {
        ...state,
        specialProducts: payload,
      };

    case ADD_PRODUCTS_TO_CART:
      return {
        ...state,
        cart: [...state.cart, payload],
      };
    case INCRASE_AMOUNT_IN_CART:
      return {
        ...state,
        cart: payload,
      };
    case DECRASE_AMOUNT_IN_CART:
      return {
        ...state,
        cart: payload,
      };
    case REMOVE_PRODUCTS_FROM_CART:
      return {
        ...state,
        cart: payload,
      };
    // case REMOVE_PRODUCTS_FROM_CART:
    //   return {
    //     ...state,
    //     cart: state.cart.filter((item) => item.id !== payload.id && item.sizeId !== payload.sizeId),
    //   };
    case LOAD_CATEGORIES:
      return {
        ...state,
        categories: payload,
      };
    default:
      return state;
  }
};

// if (exist) {
//   //postoji
//   if (exist.sizeId === item.sizeId) {
//     //ista velicina
//     let existId = state.cart.findIndex((i) => i.id === exist.id);
//     modifiedItem = { ...exist, amount: exist.amount + 1 };
//     lsItems[existId].amount = lsItems[existId].amount + 1;
//     dispatch({
//       type: INCRASE_AMOUNT_IN_CART,
//       payload: lsItems,
//     });
//     //razlicita velicina
//   } else {
//     if (item.amount) {
//       modifiedItem = item;
//     } else {
//       modifiedItem = { ...item, amount: 1 };
//     }
//     lsItems.push(modifiedItem);

//     dispatch({
//       type: ADD_PRODUCTS_TO_CART,
//       payload: modifiedItem,
//     });
//   }
//   //ne postoji
// } else {
//   if (item.amount) {
//     modifiedItem = item;
//   } else {
//     modifiedItem = { ...item, amount: 1 };
//   }
//   lsItems.push(modifiedItem);

//   dispatch({
//     type: ADD_PRODUCTS_TO_CART,
//     payload: modifiedItem,
//   });
// }

// let storageItem = [...lsItems];
// state.cart = storageItem;
// localStorage.setItem("tudorsCart", JSON.stringify(storageItem));
