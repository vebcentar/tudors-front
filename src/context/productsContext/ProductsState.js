import React, { useReducer } from "react";
import productsReducer from "./productsReducer";
import ProductsContext from "./productstContext";

import {
  LOAD_PRODUCTS,
  REMOVE_PRODUCTS_FROM_CART,
  ADD_PRODUCTS_TO_CART,
  INCRASE_AMOUNT_IN_CART,
  DECRASE_AMOUNT_IN_CART,
  LOAD_SPECIAL_PRODUCTS,
  LOAD_CATEGORIES,
} from "../types";
import axios from "axios";

const ProductState = (props) => {
  const intialState = {
    products: [],
    specialProducts: [],
    cart: JSON.parse(localStorage.getItem("tudorsCart")) || [],
    searchedProducts: null,
    categories: [],
  };
  const [state, dispatch] = useReducer(productsReducer, intialState);

  //load products
  const loadProducts = async (res) => {
    dispatch({
      type: LOAD_PRODUCTS,
      payload: res,
    });
  };

  const loadSpecialProducts = async (res) => {
    dispatch({
      type: LOAD_SPECIAL_PRODUCTS,
      payload: res,
    });
  };

  const loadCategories = async (res) => {
    dispatch({
      type: LOAD_CATEGORIES,
      payload: res,
    });
  };

  const searchProducts = async (id) => {
    const url = `${process.env.REACT_APP_TUDORS_API}api/products/getAllProducts`;

    const data = {
      offset: 0,
      limit: 20,
      category_id: 0,
      product_code: id,
    };
    try {
      const res = await axios.post(url, data);
      console.log("*************************");
      console.log("SEAERCHED DATA");
      console.log("*************************");
      console.log(res);

      // searchProducts = [...searchProducts, res.data.products];
      // searchProducts = res.data.products;
      dispatch({
        type: LOAD_PRODUCTS,
        payload: res.data.products,
      });
    } catch (err) {
      console.log(err);
    }
  };

  //cart
  const clearCart = () => {
    localStorage.removeItem("tudorsCart");
    intialState.cart = [];
  };

  const removeFromCart = (item) => {
    console.log(item);
    const lsItems = JSON.parse(localStorage.getItem("tudorsCart")) || [];
    if (lsItems.length === 0) return;
    let exist = lsItems.find((i) => i.id === item.id && i.sizeId === item.sizeId);
    let existId = state.cart.findIndex((i) => i.id === exist.id && i.sizeId === exist.sizeId);
    lsItems.splice(existId, 1);
    console.log(existId);
    dispatch({
      type: REMOVE_PRODUCTS_FROM_CART,
      payload: lsItems,
    });
    localStorage.setItem("tudorsCart", JSON.stringify(lsItems));
  };

  const decraseAmount = (item) => {
    console.log(item);
    const lsItems = JSON.parse(localStorage.getItem("tudorsCart")) || [];

    if (lsItems.length === 0) return;

    let exist = lsItems.find((i) => i.id === item.id && i.sizeId === item.sizeId);
    let existId = state.cart.findIndex((i) => i.id === exist.id);
    lsItems[existId].amount = lsItems[existId].amount - 1;
    localStorage.setItem("tudorsCart", JSON.stringify(lsItems));
    dispatch({
      type: DECRASE_AMOUNT_IN_CART,
      payload: lsItems,
    });
  };

  const addToCart = (item) => {
    // console.log("VELICINA IZ PRODUXT STATE" + item.size);
    const lsItems = JSON.parse(localStorage.getItem("tudorsCart")) || [];

    let exist = lsItems.find((i) => i.id === item.id && i.sizeId === item.sizeId);

    if (exist) {
      let existId = state.cart.findIndex((i) => i.id === exist.id && i.sizeId === item.sizeId);
      lsItems[existId].amount = lsItems[existId].amount + 1;
      dispatch({
        type: INCRASE_AMOUNT_IN_CART,
        payload: lsItems,
      });
    } else {
      // item.amount = 1;
      lsItems.push(item);
      dispatch({
        type: INCRASE_AMOUNT_IN_CART,
        payload: lsItems,
      });
    }

    let storageItem = [...lsItems];
    // state.cart = storageItem;
    localStorage.setItem("tudorsCart", JSON.stringify(storageItem));
  };

  return (
    <ProductsContext.Provider
      value={{
        specialProducts: state.specialProducts,
        products: state.products,
        cart: state.cart,
        searchedProducts: state.searchedProducts,
        categories: state.categories,
        loadProducts,
        removeFromCart,
        addToCart,
        decraseAmount,
        searchProducts,
        clearCart,
        loadCategories,
        loadSpecialProducts,
      }}>
      {props.children}
    </ProductsContext.Provider>
  );
};

export default ProductState;
